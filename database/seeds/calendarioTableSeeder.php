<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class calendarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/*Calendarios padres*/
        DB::table('calendario')->insert([
        	'usuario' => '2',
        	'mes' => '08',
        	'anio' => '2019',
            'contenido' => '/storage/uploads/calendarios/01papas AGOSTO 2019.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '2',
        	'mes' => '09',
        	'anio' => '2019',
            'contenido' => '/storage/uploads/calendarios/02papas SEPTIEMBRE 2019.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '2',
        	'mes' => '10',
        	'anio' => '2019',
            'contenido' => '/storage/uploads/calendarios/03papas OCTUBRE 2019.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '2',
        	'mes' => '11',
        	'anio' => '2019',
            'contenido' => '/storage/uploads/calendarios/04papas NOVIEMBRE 2019.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '2',
        	'mes' => '12',
        	'anio' => '2019',
            'contenido' => '/storage/uploads/calendarios/05papas DICIEMBRE 2019.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '2',
        	'mes' => '01',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/06papas ENERO 2020.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '2',
        	'mes' => '02',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/07papas FEBRERO 2020.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '2',
        	'mes' => '03',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/08papas MARZO 2020.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '2',
        	'mes' => '04',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/09papas ABRIL 2020.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '2',
        	'mes' => '05',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/10papas MAYO 2020.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '2',
        	'mes' => '06',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/11papas JUNIO 2020.pdf',
        ]);

        /*Calenadrio maestros*/
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '08',
        	'anio' => '2019',
            'contenido' => '/storage/uploads/calendarios/01maestrosAGOSTO 2019.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '09',
        	'anio' => '2019',
            'contenido' => '/storage/uploads/calendarios/02maestrosSEPTIEMBRE 2019.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '10',
        	'anio' => '2019',
            'contenido' => '/storage/uploads/calendarios/03maestrosOCTUBRE2019.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '11',
        	'anio' => '2019',
            'contenido' => '/storage/uploads/calendarios/04maestrosNOVIEMBRE 2019.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '12',
        	'anio' => '2019',
            'contenido' => '/storage/uploads/calendarios/05maestrosDICIEMBRE 2019.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '01',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/06maestrosENERO 2020.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '02',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/07maestrosFEBRERO 2020.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '03',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/08maestrosMARZO 2020.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '04',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/09maestrosABRIL 2020.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '05',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/10maestrosMAYO 2020.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '06',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/11maestrosJUNIO 2020.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '07',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/12maestrosJULIO 2020.pdf',
        ]);
        DB::table('calendario')->insert([
        	'usuario' => '3',
        	'mes' => '08',
        	'anio' => '2020',
            'contenido' => '/storage/uploads/calendarios/13maestrosAGOSTO2020.pdf',
        ]);
    }
}
