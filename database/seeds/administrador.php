<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class administrador extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'Inlakesh',
        	'email' => 'inlakesh.contacto@gmail.com',
        	'password' => Hash::make('12345678'),
            'id_rol' => '1',
        ]);
        DB::table('administradors')->insert([
            'name' => 'Inlakesh',
            'email' => 'inlakesh.contacto@gmail.com',
            'password' => Hash::make('12345678'),
        ]);
        DB::table('users')->insert([
            'name' => 'Arely',
            'email' => 'arely@touchesbegan.com',
            'password' => Hash::make('12345678'),
            'id_rol' => '2',
        ]);
        DB::table('tutores')->insert([
            'name' => 'Arely',
            'email' => 'arely@touchesbegan.com',
            'sswor' => Hash::make('12345678'),
            'id_user' => '2',
        ]);
        DB::table('users')->insert([
            'name' => 'Kennia',
            'email' => 'kennia@touchesbegan.com',
            'password' => Hash::make('12345678'),
            'id_rol' => '3',
        ]);
        DB::table('profesor')->insert([
            'name' => 'Kennia',
            'email' => 'kennia@touchesbegan.com',
            'sswor' => Hash::make('12345678'),
            'fechaNacimiento' => '16/01/1993',
            'nivel' => 'Jardín',
            'grado' => 'Bosque',
            'grupo' => 'A',
            'id_user' => '3',
        ]);
        DB::table('alumnos')->insert([
            'name' => 'Angelica Ramirez Jarquin',
            'fechaNacimiento' => '16/01/2015',
            'nivel' => 'Jardín',
            'grado' => 'Bosque',
            'grupo' => 'A',
            'tutor1' => '1',
        ]);
    }
}
