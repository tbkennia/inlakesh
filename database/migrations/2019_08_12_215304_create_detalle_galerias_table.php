<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleGaleriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_galerias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('foto');
            $table->unsignedBigInteger('idgaleria');
            $table->foreign('idgaleria')->references('id')->on('galerias')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_galerias');
    }
}
