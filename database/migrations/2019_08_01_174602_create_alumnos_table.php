<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('fechaNacimiento');
            $table->string('nivel');
            $table->string('grado');
            $table->string('grupo');
            $table->unsignedBigInteger('tutor1')->nullable();
            $table->foreign('tutor1')->references('id')->on('tutores')->onDelete('cascade');
            $table->unsignedBigInteger('tutor2')->nullable();
            $table->foreign('tutor2')->references('id')->on('tutores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
