<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class administrador extends Model
{
    protected $table = 'administradors';
    protected $primaryKey = 'id';
}
