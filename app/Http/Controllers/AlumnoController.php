<?php

namespace App\Http\Controllers;

use App\alumno;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlumnoController extends Controller
{
    public function __construct()
    {
        $this->middleware('Administrador');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function crear_alumno(Request $request)
    {
        //var_dump($request->get('asignar'));
        //var_dump($request->get('anadir'));
        $alumno = new alumno;
        $alumno['name'] = $request->get('nombre');
        $alumno['fechaNacimiento'] = $request->get('edad');
        $alumno['nivel'] = $request->get('grado_nivel');
        $alumno['grado'] = $request->get('grado');
        $alumno['grupo'] = $request->get('grupo');
        //var_dump($alumno['name']);
        //var_dump($alumno['edad']);
        //var_dump($alumno['grado_nivel']);
        //var_dump($alumno['grado']);
        //var_dump($alumno['grupo']);
        //var_dump($alumno);
        $alumno->save();
        $identificador =$alumno['id'];
        //var_dump($id);
        //return redirect('/asignar_tutor', compact('identificador'));
        if ($request->get('asignar') == "Asignar tutor(es) existente(s)") {
            return redirect()->route('asignar_tutor', $identificador);
        }if ($request->get('anadir') == "Añadir nuevo(s) tutor(es)") {
            return redirect()->route('registrar_tutor', $identificador);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function show(alumno $alumno)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function edit(alumno $alumno)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, alumno $alumno)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function destroy(alumno $alumno)
    {
        //
    }
}
