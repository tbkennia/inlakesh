<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\roles;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/alumnos';

    
    public function redirectPath()
    {
        $user = User::all();
        $rol = roles::all();

        if(auth()->user()->id_rol == '1'){
            return '/calendario_administrador';
        } elseif (auth()->user()->id_rol == '2') {
            return '/calificaciones';
        } elseif (auth()->user()->id_rol == '3') {
            return '/calendario_profesor';
        }
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
