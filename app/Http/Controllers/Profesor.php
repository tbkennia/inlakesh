<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Calendario;
use App\MesCalendario;
use App\galeria;

class Profesor extends Controller
{
    public function __construct()
    {
        //
    }
    public function index(){
        $calendario_anio = DB::table('calendario')->select('anio')->distinct()->where('usuario', "=", 3)->orderBy('anio', 'desc')->get();
        $calendario = DB::table('calendario')->where('usuario', "=", 3)->orderBy('anio', 'desc')->orderBy('mes', 'desc')->get();
        if (count($calendario)<1) {
            return redirect('alumnos_profesor');
        }else{
            return view('/profesores/calendario_profesor',compact('calendario','calendario_anio'));
        }
    }
    public function alumnos_profesor(){
        $user = auth()->user()->id;
        $nivel = DB::select("select nivel from profesor where id_user = {$user}");
        $grado = DB::select("select grado from profesor where id_user = {$user}");
        $grupo = DB::select("select grupo from profesor where id_user = {$user}");
        $lista = DB::select("select * from alumnos where nivel = '{$nivel[0]->nivel}' and grado = '{$grado[0]->grado}' and grupo = '{$grupo[0]->grupo}'");
        $tutor = DB::select('select * from tutores');
        $profesor = DB::table('profesor')->get();
        return view('/profesores/alumnos_profesor')->with(compact('lista', 'tutor', 'profesor'));
    }
    public function boletin_profesor(){
    	$mesCalendario = MesCalendario::all();
        if (count($mesCalendario)<1) {
            return redirect('calendario_profesor');
        }else{
            $ulti  = mesCalendario::orderBy('id', 'desc')->first()->id;
            $ultimo = DB::table('mesBoletin')->where('id', "=", $ulti)->get();
            $mes = DB::table('mesBoletin')->where('id', "<", $ulti)->orderBy('id', 'desc')->get();
            $btn_activo = DB::table('boletines')->orderBy('id', 'desc')->where('idMesBoletin', '=', $ulti)->get();
            $btnes = DB::table('boletines')->orderBy('id', 'desc')->where('idMesBoletin', '!=', $ulti)->get();
            return view('/profesores/boletin_profesor')->with(compact('mes', 'ultimo', 'btn_activo', 'btnes'));
        }
    	return view('/profesores/boletin_profesor');
    }
    public function calificaciones_alumno($id){
    	return view('/profesores/calificaciones_alumno');
    }
    public function documentos_profesor(){
        $docu5 = DB::table('documentos')->where('titulo', '=', "Seguimiento a reuniones")->orderBy('id', 'desc')->limit(1)->get();

        $docu6 = DB::table('documentos')->where('titulo', '=', "Compras")->orderBy('id', 'desc')->limit(1)->get();
        
        $docu7 = DB::table('documentos')->where('titulo', '=', "Evaluaciones")->orderBy('id', 'desc')->limit(1)->get();

        $docu8 = DB::table('documentos')->where('titulo', '=', "Autoevaluaciones")->orderBy('id', 'desc')->limit(1)->get();
       
        if (count($docu5) < 1) {
            $temporal = (object) array('contenido' => '#');
            $docu5 = array($temporal);
        }if (count($docu6) < 1) {
            $temporal = (object) array('contenido' => '#');
            $docu6 = array($temporal);
        }if (count($docu7) < 1) {
            $temporal = (object) array('contenido' => '#');
            $docu7 = array($temporal);
        }if (count($docu8) < 1) {
            $temporal = (object) array('contenido' => '#');
            $docu8 = array($temporal);
        }
        return view('/profesores/documentos_profesor')->with(compact('docu5', 'docu6', 'docu7', 'docu8'));
    }
    public function editar_calificaciones(){
    	return view('/profesores/editar_calificaciones');
    }
    public function galeria_profesor(){
    	$galery = DB::table('galerias')->orderBy('id', 'desc')->get();
        return view('/profesores/galeria_profesor')->with('galery', $galery);
    }
    public function vista_galeria_profesor($id){
    	$vista_fotos = DB::table('detalle_galerias')->where('idgaleria', '=', $id)->get();
        $galery = DB::table('galerias')->where('id', '=', $id)->get();
        return view('/profesores/vista_galeria_profesor')->with(compact('id', 'vista_fotos', 'galery'));
    }
    public function registrar_calificacion(){
    	return view('/profesores/registrar_calificacion');
    }
    public function avisos_profesor(){
        $avisos = DB::table('avisos')->orderBy('id', 'desc')->get();
        return view('profesores/avisos_profesor')->with('avisos', $avisos);
    }
    public function videos_profesor(){
        return view('/profesores/videos');
    }
    
}
