<?php

namespace App\Http\Controllers;

use File;
use Mail;
use Session;
use App\User;
use App\avisos;
use App\tutore;
use App\alumno;
use App\galeria;
use App\profesor;
use App\boletines;
use App\documentos;
use App\Calendario;
use App\administrador;
use App\MesCalendario;
use App\detalle_galeria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class AdministradorController extends Controller
{
    public function __construct()
    {
        $this->middleware('Administrador');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //peticiones get
    public function index()
    {
        $calendario_usuario = DB::table('calendario')->select('usuario')->distinct()->get();
        $calendario_anio = DB::table('calendario')->select('anio')->distinct()->orderBy('anio', 'desc')->get();
        $calendario = DB::table('calendario')->orderBy('mes', 'desc')->orderBy('anio', 'desc')->get();
        return view('/administrador/calendario_administrador')->with(compact('calendario', 'calendario_anio', 'calendario_usuario'));
    }
    public function eliminar_calendario()
    {
        $calendario_usuario = DB::table('calendario')->select('usuario')->distinct()->get();
        $calendario_anio = DB::table('calendario')->select('anio')->distinct()->orderBy('anio', 'desc')->get();
        $calendario = DB::table('calendario')->orderBy('anio', 'desc')->orderBy('mes', 'desc')->get();
        return view('/administrador/eliminar_calendario')->with(compact('calendario', 'calendario_anio', 'calendario_usuario'));
    }
    public function eliminar_contenido_calendario(Request $request){
        $calendario = DB::table('calendario')->where('id', '=', $request->get('idCalendario'))->get();
        $calendar = public_path($calendario[0]->contenido);
        if (file_exists($calendar)) {
            unlink($calendar);
        }
        DB::table('calendario')->delete($request->get('idCalendario'));
        return back();
    }
    public function crear_calendario(){
        return view('/administrador/crear_calendario');
    }
    public function editar_calendario(){
        return view('/administrador/editar_calendario');
    }
    public function ver_calendario(){
        return view('/administrador/ver_calendario');
    }
    public function agregar_galeria()
    {
        return view('/administrador/agregar_galeria');
    }
    public function videos_administrador()
    {
        return view('/administrador/videos');
    }
    public function galeria_administrador()
    {
        $galery = DB::table('galerias')->orderBy('id', 'desc')->get();
        return view('/administrador/galeria_administrador')->with('galery', $galery);
    }
    public function agregar_fotos_galeria($id)
    {
        $infoGalery = DB::table('galerias')->where('id', '=', $id)->get();
        return view('administrador/agregar_fotos_galeria')->with(compact('id', 'infoGalery'));
    }
    public function guardar_fotos_galería(Request $request){
        foreach ($request->photos as $key) {
            $carpeta = '/G_'.$request->get('idGaleria');
            $url_galery = $key->store('public/uploads/galeria'.$carpeta.'');
            $sep = explode('/', $url_galery);
            $url_final = '/storage/'.$sep[1].'/'.$sep[2].'/'.$sep[3].'/'.$sep[4];
            $fotos_galeria = new detalle_galeria;
            $fotos_galeria['foto'] = $url_final;
            $fotos_galeria['idgaleria'] = $request->get('idGaleria');
            $fotos_galeria->save();
        }
        return redirect('galeria_administrador');
    }
    public function guardar_galeria(Request $request){
        $url_galery = $request->file('flip_img_2')->store('public/uploads/galeria');
        $sep = explode('/', $url_galery);
        $url_final = '/storage/'.$sep[1].'/'.$sep[2].'/'.$sep[3];
        $galeria = new galeria;
        $galeria['titulo'] = $request->get('name_galery');
        $galeria['foto'] = $url_final;
        $galeria['descripcion'] = $request->get('desc');
        $galeria->save();
        return redirect('galeria_administrador');
    }
    public function editar_galeria_administrador()
    {
        $galery = galeria::all();
        return view('/administrador/editar_galeria_administrador')->with('galery', $galery);
    }
    public function vista_galeria_administrador($id)
    {
        $vista_fotos = DB::table('detalle_galerias')->where('idgaleria', '=', $id)->get();
        $galery = DB::table('galerias')->where('id', '=', $id)->get();
        return view('/administrador/vista_galeria_administrador')->with(compact('id', 'vista_fotos', 'galery'));
    }
    public function eliminar_galeria(){
        $galery = galeria::all();
        return view('/administrador/eliminar_galeria')->with('galery', $galery);
    }
    public function eliminar_contenido_galeria(Request $request)
    {
        $ruta = DB::table('galerias')->where('id', '=', $request->get('idGaleria'))->get();
        $galeria = $ruta[0]->id;
        $directorio = DB::table('detalle_galerias')->where('idgaleria', '=', $galeria)->get();

        if (count($directorio) > 0) {

            foreach ($directorio as $fotito) {
                $image_galery = public_path($fotito->foto);
                if (file_exists($image_galery)) {
                    unlink($image_galery);
                }
            }
            $dir = $directorio[0]->foto;
            $sep = explode('/', $dir);
            $url_final = $sep[1].'/'.$sep[2].'/'.$sep[3].'/'.$sep[4];
            $folder = public_path($url_final);

            if (is_dir($folder)) {
                rmdir($folder);
            }
        }else{
            //
        }

        $image = public_path($ruta[0]->foto);
        if (file_exists($image)) {
            unlink($image);
        }
        DB::table('galerias')->delete($request->get('idGaleria'));
        return back();
    }
    public function boletin_administrador()
    {
        $mesCalendario = MesCalendario::all();
        if (count($mesCalendario)<1) {
            return redirect('crear_mes');
        }else{
            $ulti  = mesCalendario::orderBy('id', 'desc')->first()->id;
            $ultimo = DB::table('mesBoletin')->where('id', "=", $ulti)->get();
            $mes = DB::table('mesBoletin')->where('id', "<", $ulti)->orderBy('id', 'desc')->get();
            $btn_activo = DB::table('boletines')->orderBy('id', 'desc')->where('idMesBoletin', '=', $ulti)->get();
            $btnes = DB::table('boletines')->orderBy('id', 'desc')->where('idMesBoletin', '!=', $ulti)->get();
            return view('/administrador/boletin_administrador')->with(compact('mes', 'ultimo', 'btn_activo', 'btnes'));
       }
    }
    public function agregar_boletin($id){
        return view('administrador/agregar_boletin')->with('id', $id);
    }
    public function guardar_boletin(Request $request){
        
        $url_btin = $request->file('flip_img_1')->store('public/uploads/boletines');
        $sep = explode('/', $url_btin);
        $url_final = '/storage/'.$sep[1].'/'.$sep[2].'/'.$sep[3];

        $boletin = new boletines;
        $boletin['titulo'] = $request->get('titulo');
        $boletin['contenido'] = $url_final;
        $boletin['idMesBoletin'] = $request->get('iden');
        $boletin->save();
        return redirect('boletin_administrador');
    }
    public function crear_mes(){
        return view('/administrador/crear_mes');
    }
    public function guardar_mes(Request $request){
        $mesCalendario = new MesCalendario;
        $mesCalendario['mes'] = $request->get('mes');
        $mesCalendario['anio'] = $request->get('anio');
        $mesCalendario->save();
        return redirect('boletin_administrador');
    }
    public function eliminar_mes(){

        $mes = MesCalendario::all();
        return view('/administrador/eliminar_mes')->with('mes', $mes);
    }
    public function eliminar_contenido_mes(Request $request)
    {
        $btnes_ruta = DB::table('boletines')->where('idMesBoletin', '=', $request->get('mes'))->get();
        foreach ($btnes_ruta as $ruta) {
            $boletin = public_path($ruta->contenido);
            if (file_exists($boletin)) {
                unlink($boletin);
            }
        }

        DB::table('mesBoletin')->delete($request->get('mes'));
        return back();
    }
    public function alumnos()
    {
        return view('/administrador/alumnos');
    }
    public function editar_alumno($id)
    {
        $yaExiste = "";
        $ide_edit_alu = $id;
        $alumno = alumno::where('id', '=', $ide_edit_alu)->first();
        $tutor = DB::select("select * from tutores");

        $id_tutor1 = DB::select("select tutor1 from alumnos where id = '{$ide_edit_alu}'");
        if ($id_tutor1[0]->tutor1 == "") {
            $tutor1_id = "";
            $tutor1_name = "";
            $tutor1_mail = "";
        }else{
            $name_tutor1 = DB::select("select * from tutores where id = '{$id_tutor1[0]->tutor1}'");
            $tutor1_id = $name_tutor1[0]->id;
            $tutor1_name = $name_tutor1[0]->name;
            $tutor1_mail = $name_tutor1[0]->email;
        }
        $id_tutor2 = DB::select("select tutor2 from alumnos where id = '{$ide_edit_alu}'");
        if ($id_tutor2[0]->tutor2 == "") {
            $tutor2_id = "";
            $tutor2_name = "";
            $tutor2_mail = "";
        }else{
            $name_tutor2 = DB::select("select * from tutores where id = '{$id_tutor2[0]->tutor2}'");
            $tutor2_id = $name_tutor2[0]->id;
            $tutor2_name = $name_tutor2[0]->name;
            $tutor2_mail = $name_tutor2[0]->email;
        }

        return view('/administrador/editar_alumno')->with(compact('alumno', 'tutor', 'tutor1_id', 'tutor1_name', 'tutor1_mail', 'tutor2_id', 'tutor2_name', 'tutor2_mail', 'yaExiste'));

    }
    public function actualiza_alumno(Request $request){
        $alumno['id'] = $request->get('alumno');
        $alumno = alumno::where('id', '=', $alumno['id'])->first();
        if($alumno == true){
            $alumno['name'] = $request->get('name');
            $alumno['fechaNacimiento'] = $request->get('fechaNacimiento');
            $alumno['nivel'] = $request->get('nivelgdo');
            $alumno['grado'] = $request->get('grado');
            $alumno['grupo'] = $request->get('grupo');
            $alumno->save();
        }
        return view('/administrador/alumnos');
    }
    public function lista($grupo)
    {
        $url_boleta = Storage::url('uploads/boletas/primaria/primero/InlakeshBoleta_Primaria12017.pdf');
        //Jardín
        if ($grupo == "JCA") {
            $alumno = DB::select('select * from alumnos where nivel = "Jardín" and grado = "Capullo" and grupo = "A"');
        }else if ($grupo == "JSA") {
            $alumno = DB::select('select * from alumnos where nivel = "Jardín" and grado = "Semilla" and grupo = "A"');
        }elseif ($grupo == "JBA") {
            $alumno = DB::select('select * from alumnos where nivel = "Jardín" and grado = "Bosque" and grupo = "A"');
        }elseif ($grupo == "JLA") {
            $alumno = DB::select('select * from alumnos where nivel = "Jardín" and grado = "Luna" and grupo = "A"');
        }

        //Primaria
        elseif($grupo == "P1A") {
            $alumno = DB::select('select * from alumnos where nivel = "Primaria" and grado = "1" and grupo = "A"');
        }elseif($grupo == "P2A") {
            $alumno = DB::select('select * from alumnos where nivel = "Primaria" and grado = "2" and grupo = "A"');
        }elseif($grupo == "P3A") {
            $alumno = DB::select('select * from alumnos where nivel = "Primaria" and grado = "3" and grupo = "A"');
        }elseif($grupo == "P4A") {
            $alumno = DB::select('select * from alumnos where nivel = "Primaria" and grado = "4" and grupo = "A"');
        }elseif($grupo == "P5A") {
            $alumno = DB::select('select * from alumnos where nivel = "Primaria" and grado = "5" and grupo = "A"');
        }

        //Secundaria 1°
        elseif($grupo == "S6A") {
            $alumno = DB::select('select * from alumnos where nivel = "Secundaria" and grado = "6" and grupo = "A"');
        }elseif($grupo == "S7A") {
            $alumno = DB::select('select * from alumnos where nivel = "Secundaria" and grado = "7" and grupo = "A"');
        }elseif($grupo == "S8A") {
            $alumno = DB::select('select * from alumnos where nivel = "Secundaria" and grado = "8" and grupo = "A"');
        }elseif($grupo == "S9A") {
            $alumno = DB::select('select * from alumnos where nivel = "Secundaria" and grado = "9" and grupo = "A"');
        }
        $tutor = DB::select('select * from tutores');
        return view('/administrador/lista')->with(compact('alumno', 'tutor', 'url_boleta'));
    }
    public function lista_vacia($grupo)
    {
        return view('/administrador/lista_vacia');
    }
     public function registrar_alumno()
    {
        return view('/administrador/registrar_alumno');
    }
    public function tutores()
    {
        $done = "";
        $tutor = tutore::all();
        $tutelado = alumno::all();
        return view('/administrador/tutores')->with(compact('tutor', 'tutelado', 'done'));
    }
    public function crear_tutor(Request $request)
    {
        $mail = $request->get('email');
        $data = array('user' => $request->get('email'), 'password' => $request->get('password'), 'matricula' => $request->get('matricula'));
        Mail::send('/mailing/mail_tutor', $data, function($msj) use($mail){
            $msj->subject('Datos de acceso Inlakesh');
            $msj->to($mail);
        });

        Session::flash('message', 'Mensaje enviado correctamente');
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        $tutor = new tutore;
        $tutor['name'] = $request->get('name');
        $tutor['email'] = $request->get('email');
        $pass = $request->get('password');
        $tutor['sswor'] = $pass;
        $tutor['id_user'] = $user['id'];

        $tutor->save();
        $identificador =$tutor['id'];
        $alumno['id'] = $request->get('alumno');
        $alumno = alumno::where('id', '=', $alumno['id'])->first();

        $tutorAlumno = alumno::where('id', '=', $alumno['id'])->first();
        print_r($tutorAlumno->tutor1);

        if ($tutorAlumno->tutor1 == "") {
            if($alumno == true){
                $alumno['tutor1'] = $identificador;
                $alumno->save();
            }
        }else if ($tutorAlumno->tutor1 != "") {
            if($alumno == true){
                $alumno['tutor2'] = $identificador;
                $alumno->save();
            }
        }

        if ($request->get('solo_uno') == "Registrar solo un tutor") {
            return redirect('/tutores');
        }if ($request->get('agregar_otro') == "Añadir otro tutor") {
            $identificador = $request->get('alumno');
            return redirect()->route('registrar_tutor2', $identificador);
        }if ($request->get('registra_otro') == "Registrar") {
            return redirect('/tutores');
        }   
    }
    public function editar_tutor($id)
    {
        $tutor = tutore::where('id', '=', $id)->first();
        $tutelado1 = DB::select("select * from alumnos where tutor1 = '{$tutor->id}'");
        $tutelado2 = DB::select("select * from alumnos where tutor2 = '{$tutor->id}'");
        $alumno = DB::table('alumnos')->get();
        return view('/administrador/editar_tutor')->with(compact('tutor', 'tutelado1', 'tutelado2', 'alumno'));
    }
    public function actualiza_tutor(Request $request){
        $tutor['id'] = $request->get('tutor');
        $tutor = tutore::where('id', '=', $tutor['id'])->first();
        if($tutor == true){
            $tutor['name'] = $request->get('name');
            $tutor['email'] = $request->get('email');
            $tutor->save();
        }
        return redirect('tutores');
    }
    public function tutor_asignado(Request $request)
    {
        $alumno['id'] = $request->get('alumno');
        $alumno = alumno::where('id', '=', $alumno['id'])->first();
        if($alumno == true){
            $alumno['tutor1'] = $request->get('matricula');
            if ($request->get('matricula2') != "") {
                $alumno['tutor2'] = $request->get('matricula2');
            }
            $alumno->save();
        }
        return redirect('/tutores');
    }
    public function registrar_tutor($id)
    {
        $tutor = tutore::all();
        if (count($tutor)<1) {
            $sum = 1;
        }else{
            $sum = $tutor->last()->id + 1;
        }
        $in = "<div class='row' id='dosPapas'>
                <div class='input-field col s12'>
                    <input type='submit' name='solo_uno' value='Registrar solo un tutor' class='btn waves-effect waves-light col s12 fondo-lila texto-amarillo'>
                    </div>
                </div>
                <div class='row' id='dosPapas'>
                  <div class='input-field col s12'>
                    <input type='submit' name='agregar_otro' value='Añadir otro tutor' class='btn waves-effect waves-light col s12 fondo-lila texto-amarillo'>
                  </div>
                </div>";
        return view('/administrador/registrar_tutor')->with(compact('sum', 'id', 'in'));
    }
    public function registrar_tutor2($id)
    {
        $tutor = tutore::all();
        if (count($tutor)<1) {
            $sum = 1;
        }else{
            $sum = $tutor->last()->id + 1;
        }
        $in = "<div class='row' id='unPapa'>
                    <div class='input-field col s12'>
                        <input type='submit' name='registra_otro' value='Registrar' class='btn waves-effect waves-light col s12 fondo-lila texto-amarillo'>
                    </div>
                </div>";
        return view('/administrador/registrar_tutor')->with(compact('sum', 'id', 'in'));
    }
    public function establecer_tutor(Request $request){
        $yaExiste = "";
        $alumno = $request->get('alumno');

        $noHayTutor1 = DB::table('alumnos')->where('id', $alumno)->where('tutor1', NULL)->get();
        $noHayTutor2 = DB::table('alumnos')->where('id', $alumno)->where('tutor2', NULL)->get();
        
        if ($request->get('nivel_tutor') == 1) {
            if (count($noHayTutor1) > 0) {
                DB::table('alumnos')->where('id', $alumno)->update(['tutor1' => $request->get('tutor_existente')]);
            }else if (count($noHayTutor1) == 0) {
                $yaExiste = "<p style='color:red; float: right;'><i class=material-icons>warning</i> Ya existe un tutor principal, para cambiarlo dede 'Dejar de ser tutor', el actual asignado</p>";
            }
        }
        if ($request->get('nivel_tutor') == 2) {
            if (count($noHayTutor2) > 0) {
                DB::table('alumnos')->where('id', $alumno)->update(['tutor2' => $request->get('tutor_existente')]);
            }else {
                $yaExiste = "<p style='color:red; float: right;'><i class=material-icons>warning</i> Ya existe un segundo tutor, para cambiarlo dede 'Dejar de ser tutor', el actual asignado</p>";
            }
        }

        $ide_edit_alu = $request->get('alumno');
        $alumno = alumno::where('id', '=', $ide_edit_alu)->first();
        $tutor = DB::select("select * from tutores");

        $id_tutor1 = DB::select("select tutor1 from alumnos where id = '{$ide_edit_alu}'");
        if ($id_tutor1[0]->tutor1 == "") {
            $tutor1_id = "";
            $tutor1_name = "";
            $tutor1_mail = "";
        }else{
            $name_tutor1 = DB::select("select * from tutores where id = '{$id_tutor1[0]->tutor1}'");
            $tutor1_id = $name_tutor1[0]->id;
            $tutor1_name = $name_tutor1[0]->name;
            $tutor1_mail = $name_tutor1[0]->email;
        }
        $id_tutor2 = DB::select("select tutor2 from alumnos where id = '{$ide_edit_alu}'");
        if ($id_tutor2[0]->tutor2 == "") {
            $tutor2_id = "";
            $tutor2_name = "";
            $tutor2_mail = "";
        }else{
            $name_tutor2 = DB::select("select * from tutores where id = '{$id_tutor2[0]->tutor2}'");
            $tutor2_id = $name_tutor2[0]->id;
            $tutor2_name = $name_tutor2[0]->name;
            $tutor2_mail = $name_tutor2[0]->email;
        }

        return view('/administrador/editar_alumno')->with(compact('alumno', 'tutor', 'tutor1_id', 'tutor1_name', 'tutor1_mail', 'tutor2_id', 'tutor2_name', 'tutor2_mail', 'yaExiste'));
    }
    public function eliminar_tutor(Request $request)
    {
        $tutor1De = DB::table('alumnos')->where('tutor1', '=', $request->get('tutor'))->get();
        $tutor2De = DB::table('alumnos')->where('tutor2', '=', $request->get('tutor'))->get();
        if (count($tutor1De) > 0) {
            DB::table('alumnos')->where('tutor1', $request->get('tutor'))->update(['tutor1' => NULL]);
        }
        if (count($tutor2De) > 0) {
            DB::table('alumnos')->where('tutor2', $request->get('tutor'))->update(['tutor2' => NULL]);
        }
        $idTutor = DB::table('tutores')->where('id', '=', $request->get('tutor'))->get();
        DB::table('tutores')->delete($request->get('tutor'));
        DB::table('users')->delete($idTutor[0]->id_user);
        return back();
    }
    public function eliminar_alumno(Request $request)
    {
        DB::table('alumnos')->delete($request->get('alumno'));
        return back();
    }
    public function relacionar_hijo_tutor(){
        $alumno = DB::table('alumnos')->get();
        $tutores = DB::table('tutores')->get();
        return view('/administrador/relacionar_hijo_tutor')->with(compact('alumno', 'tutores'));
    }
    public function asignar_tutor($id)
    {
        $alumno = DB::table('alumnos')->where('id', '=', $id)->get();
        $tutores = DB::table('tutores')->get();
        return view('/administrador/asignar_tutor')->with(compact('id', 'alumno', 'tutores'));
    }
    public function asignar_tutelado(Request $request){
        $alumno = alumno::where('id', '=', $request->get('alumno'))->first();
        if ($request->get('nivel_tutor') == 1) {
            $alumno = alumno::find($request->get('alumno'));
            $alumno->tutor1 = $request->get('tutor');
            $alumno->save();
        }elseif ($request->get('nivel_tutor') == 2) {
            $alumno = alumno::find($request->get('alumno'));
            $alumno->tutor2 = $request->get('tutor');
            $alumno->save();
        }
        return redirect()->route('editar_tutor', $request->get('tutor'));
    }
    public function dejar_de_ser_tutor(Request $request){
        $tutor = alumno::where('id', '=', $request->get('tutelado'))->first();
        if ($tutor->tutor1 == $request->get('tutor')) {
            $alumno = alumno::find($request->get('tutelado'));
            $alumno->tutor1 = NULL;
            $alumno->save();
        }elseif ($tutor->tutor2 == $request->get('tutor')) {
            $alumno = alumno::find($request->get('tutelado'));
            $alumno->tutor2 = NULL;
            $alumno->save();
        }
        if ($request->get('vista') == 1) {
            return redirect()->route('editar_tutor', $request->get('tutor'));
        }elseif ($request->get('vista') == 2) {
            return redirect()->route('editar_alumno', $request->get('tutelado'));
        }
        
    }
    public function profesores()
    {   
        $done = "";
        $profesor = DB::table('profesor')->get();
        return view('/administrador/profesores')->with(compact('profesor', 'done'));
    }
    public function registra_profesor(){
        return view('/administrador/registrar_profesor');
    }
    public function crear_profesor(Request $request){
        $mail = $request->get('email');
        $data = array('user' => $request->get('email'), 'password' => $request->get('password'), 'nivel' => $request->get('grado_nivel'), 'grado' => $request->get('grado').'°'.$request->get('grupo'));
        Mail::send('/mailing/mail_profesor', $data, function($msj) use($mail){
            $msj->subject('Datos de acceso Inlakesh');
            $msj->to($mail);
        });

        Session::flash('message', 'Mensaje enviado correctamente');

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create_profesor($request->all())));

        $profesor = new profesor;
        $profesor['name'] = $request->get('name');
        $profesor['email'] = $request->get('email');
        $profesor['fechaNacimiento'] = $request->get('edad');
        $profesor['nivel'] = $request->get('grado_nivel');
        $profesor['grado'] = $request->get('grado');
        $profesor['grupo'] = $request->get('grupo');
        $pass = $request->get('password');
        $profesor['sswor'] = $pass;
        $profesor['id_user'] = $user['id'];
        $profesor->save();
        return redirect('/profesores');
    }
    protected function create_profesor(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'id_rol' => "3",
        ]);
    }
    public function eliminar_profesor(Request $request)
    {
        $idProfesor = DB::table('profesor')->where('id', '=', $request->get('profe'))->get();
        DB::table('profesor')->delete($request->get('profe'));
        DB::table('users')->delete($idProfesor[0]->id_user);
        return back();
    }
    public function editar_profesor(Request $request)
    {
        $ide_edit_prof = $request->get('profesor');
        $prof = profesor::where('id', '=', $ide_edit_prof)->first();
        return view('/administrador/editar_profesor')->with('prof', $prof);
    }
    public function actualiza_profesor(Request $request){
        $prof['id'] = $request->get('profesor');
        $profe = profesor::where('id', '=', $prof['id'])->first();
        if($profe == true){
            $profe['name'] = $request->get('name');
            $profe['fechaNacimiento'] = $request->get('fechaNacimiento');
            $profe['nivel'] = $request->get('nivelgdo');
            $profe['grado'] = $request->get('grado');
            $profe['grupo'] = $request->get('group');
            $profe->save();
        }
        $id_usuario = DB::select("select id_user from profesor where id = '{$prof['id']}'");
        $user = User::where('id', '=', $id_usuario[0]->id_user)->first();
        if($user == true){
            $user['name'] = $request->get('name');
            $user->save();
        }
        return redirect('profesores');
    }
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'id_rol' => "2",
        ]);
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
    public function envio_mail_tutor(Request $request){
        $tabla = $request->get('tutores_profesores');
        $email = DB::select("select email from tutores");

        foreach ($email as $correo) {
            $user = $correo->email;
            $contrasenia = DB::select("select sswor from tutores where email = '{$correo->email}'");
            $password = $contrasenia[0]->sswor;
            $matricula = DB::select("select id from tutores where email = '{$correo->email}'");
            $matricula = $matricula[0]->id;
            $data = array('user' => $user, 'password' => $password, 'matricula' => $matricula);
            Mail::send('/mailing/mail_tutor', $data, function($msj) use($correo){
                $msj->subject('Datos de acceso Inlakesh');
                $msj->to($correo->email);
            });
            Session::flash('message', 'Mensaje enviado correctamente');
        }
        $cargaCompletaA = "";
        $cargaCompletaT = "";
        $cargaCompletaP = "";
        $done = "";
        $doneDos = "";
        $sendTut =  "<p style='color:green; margin-top: -50px; float: right;'>Se han enviado exitosamente los accesos a los tutores<i class=material-icons>done_all</i></p>";
        $sendProf = "";
        $accesTut = "";
        $accesProf = "";
        return view('administrador/cargar_datos')->with(compact('cargaCompletaA', 'cargaCompletaT', 'cargaCompletaP', 'done', 'doneDos', 'sendTut', 'sendProf', 'accesTut', 'accesProf'));
    }
    public function envio_mail_un_tutor(Request $request){
        $datos_tutor = DB::select("select * from tutores where id = '{$request->get('tutor')}'");
        $correo = $datos_tutor[0]->email;
        $data = array('user' => $datos_tutor[0]->email, 'password' => $datos_tutor[0]->sswor, 'matricula' => $datos_tutor[0]->id);
        Mail::send('/mailing/mail_tutor', $data, function ($m) use ($correo) {
            $m->from('inlakesh.contacto@gmail.com', 'Inlakesh');
            $m->to($correo, 'Inlakesh')->subject('Datos de acceso Inlakesh');
        });
        Session::flash('message', 'Mensaje enviado correctamente');
        $done = "<p style='color:green; float: right;'>Se han enviado exitosamente las contraseñas a:  <b>$correo</b> <i class=material-icons>done_all</i></p>";
        $tutor = tutore::all();
        $tutelado = alumno::all();
        return view('/administrador/tutores')->with(compact('tutor', 'tutelado', 'done'));
    }
    public function envio_mail_profesor(Request $request){
        $email = DB::select("select email from profesor");
        foreach ($email as $correo) {
            $user = $correo->email;
            $contrasenia = DB::select("select sswor from profesor where email = '{$correo->email}'");
            $password = $contrasenia[0]->sswor;
            $matricula = DB::select("select id from profesor where email = '{$correo->email}'");
            $matricula = $matricula[0]->id;
            $data = array('user' => $user, 'password' => $password, 'matricula' => $matricula);
            Mail::send('/mailing/mail_profesor', $data, function ($m) use ($correo) {
                $m->from('inlakesh.contacto@gmail.com', 'Inlakesh');

                $m->to($correo->email, 'Inlakesh')->subject('Datos de acceso Inlakesh');
            });
            Session::flash('message', 'Mensaje enviado correctamente');
        }
        $cargaCompletaA = "";
        $cargaCompletaT = "";
        $cargaCompletaP = "";
        $done = "";
        $doneDos = "";
        $sendTut =  "";
        $sendProf = "<p style='color:green; margin-top: -50px; float: right;'>Se han enviado exitosamente los accesos a los profesores<i class=material-icons>done_all</i></p>";
        $accesTut = "";
        $accesProf = "";
        return view('administrador/cargar_datos')->with(compact('cargaCompletaA', 'cargaCompletaT', 'cargaCompletaP', 'done', 'doneDos', 'sendTut', 'sendProf', 'accesTut', 'accesProf'));
    }
    public function envio_mail_un_profesor(Request $request){
        $datos_profe = DB::select("select * from profesor where id = '{$request->get('profe')}'");
        $correo = $datos_profe[0]->email;
        $data = array('user' => $datos_profe[0]->email, 'password' => $datos_profe[0]->sswor, 'matricula' => $datos_profe[0]->id);
        Mail::send('/mailing/mail_profesor', $data, function ($m) use ($correo) {
            $m->from('inlakesh.contacto@gmail.com', 'Inlakesh');
            $m->to($correo, 'Inlakesh')->subject('Datos de acceso Inlakesh');
        });
        Session::flash('message', 'Mensaje enviado correctamente');
        $done = "<p style='color:green; margin-top: -50px; float: right;'>Se han enviado exitosamente las contraseñas a:  <b>$correo</b> <i class=material-icons>done_all</i></p>";
        $profesor = DB::table('profesor')->get();
        return view('/administrador/profesores')->with(compact('profesor', 'done'));
    }
    public function generar_password(Request $request){
        $tabla = $request->get('tabla');

        if ($tabla == "tutores") {
            $tutores = tutore::where('sswor', '=', NULL)->get();
            foreach ($tutores as $tutor){
                $tutor->sswor = rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9);
                $tutor->save();
            }
            $cargaCompletaA = "";
            $cargaCompletaT = "";
            $cargaCompletaP = "";
            $done = "<p style='color:green; margin-top: -50px; float: right;'>Se han generado exitosamente las contraseñas<i class=material-icons>done_all</i></p>";
            $doneDos = "";
            $sendTut = "";
            $sendProf = "";
            $accesTut = "";
            $accesProf = "";
            return view('administrador/cargar_datos')->with(compact('cargaCompletaA', 'cargaCompletaT', 'cargaCompletaP', 'done', 'doneDos', 'sendTut', 'sendProf', 'accesTut', 'accesProf'));
        }else if ($tabla == "profesor") {
            $profesores = profesor::where('sswor', '=', NULL)->get();
            foreach ($profesores as $profesor){
                $profesor->sswor = rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9);
                $profesor->save();
            }
            $cargaCompletaA = "";
            $cargaCompletaT = "";
            $cargaCompletaP = "";
            $done = "";
            $doneDos = "<p style='color:green; margin-top: -50px; float: right;'>Se han generado exitosamente las contraseñas<i class=material-icons>done_all</i></p>";
            $sendTut = "";
            $sendProf = "";
            $accesTut = "";
            $accesProf = "";
            return view('administrador/cargar_datos')->with(compact('cargaCompletaA', 'cargaCompletaT', 'cargaCompletaP', 'done', 'doneDos', 'sendTut', 'sendProf', 'accesTut', 'accesProf'));
        }        
    }
    public function dar_acceso(Request $request){
        $tabla = $request->get('tabla');
        if ($tabla == "tutores") {
            $tutores = DB::table($tabla)->get();
            for ($i=0; $i < count($tutores); $i++) {
                $usuario = new User;
                $usuario['name'] = $tutores[$i]->name;
                $usuario['email'] = $tutores[$i]->email;
                $usuario['password'] = Hash::make($tutores[$i]->sswor);
                $usuario['id_rol'] = 2;
                $usuario->save();

                $iduser=User::select('id')->where('email', '=', "{$tutores[$i]->email}")->first();

                $tutor=tutore::where('email', '=', "{$tutores[$i]->email}")->first();
                $tutor->id_user = $iduser->id;
                $tutor->save();
            }
                $cargaCompletaA = "";
                $cargaCompletaT = "";
                $cargaCompletaP = "";
                $done = "";
                $doneDos = "";
                $sendTut = "";
                $sendProf = "";
                $accesTut = "<p style='color:green; margin-top: -50px; float: right;'>Se han dado accesos exitosamente a los tutores al sistema<i class=material-icons>done_all</i></p>";
                $accesProf = "";
                return view('administrador/cargar_datos')->with(compact('cargaCompletaA', 'cargaCompletaT', 'cargaCompletaP', 'done', 'doneDos', 'sendTut', 'sendProf', 'accesTut', 'accesProf'));
        }else if ($tabla == "profesor") {
            $profesores = DB::table($tabla)->get();
            for ($i=0; $i < count($profesores); $i++) {
                $usuario = new User;
                $usuario['name'] = $profesores[$i]->name;
                $usuario['email'] = $profesores[$i]->email;
                $usuario['password'] = Hash::make($profesores[$i]->sswor);
                $usuario['id_rol'] = 3;
                $usuario->save();

                $iduser=User::select('id')->where('email', '=', "{$profesores[$i]->email}")->first();

                $profesor=profesor::where('email', '=', "{$profesores[$i]->email}")->first();
                $profesor->id_user = $iduser->id;
                $profesor->save();
            }
                $cargaCompletaA = "";
                $cargaCompletaT = "";
                $cargaCompletaP = "";
                $done = "";
                $doneDos = "";
                $sendTut = "";
                $sendProf = "";
                $accesTut = "";
                $accesProf = "<p style='color:green; margin-top: -50px; float: right;'>Se han dado accesos exitosamente a los profesores al sistema<i class=material-icons>done_all</i></p>";
                return view('administrador/cargar_datos')->with(compact('cargaCompletaA', 'cargaCompletaT', 'cargaCompletaP', 'done', 'doneDos', 'sendTut', 'sendProf', 'accesTut', 'accesProf'));
        }
    }
    public function relacionar(Request $request){
        $id_alumno = $request->get('alumno');
        $alumno = alumno::where('id', '=', $id_alumno)->first();
        if($alumno == true){
            $alumno['tutor1'] = $request->get('tutor1');
            $alumno['tutor2'] = $request->get('tutor2');
            $alumno->save();
        }
        return redirect('tutores');
    }
    public function subir_db($usuario){
        return view('/administrador/subir_db')->with('usuario', $usuario);
    }
    public function cargar_datos(){
        $cargaCompletaA = "";
        $cargaCompletaT = "";
        $cargaCompletaP = "";
        $done = "";
        $doneDos = "";
        $sendTut = "";
        $sendProf = "";
        $accesTut = "";
        $accesProf = "";
        return view('administrador/cargar_datos')->with(compact('cargaCompletaA', 'cargaCompletaT', 'cargaCompletaP', 'done', 'doneDos', 'sendTut', 'sendProf', 'accesTut', 'accesProf'));
    }
    public function importar_db(Request $request){
        $url_btin = $request->file('files')->store('public/uploads');
        $sep = explode('/', $url_btin);
        $url_final = '/storage/'.$sep[1].'/'.$sep[2];

        $path = public_path($url_final);
        $lines = file($path);
        //$ultimo = array_map('utf8_encode', $lines);
        $array =  array_map('str_getcsv', $lines);
        if ($request->get('tabla') == 'alumnos') {
            for ($i=1; $i < sizeof($array); $i++) { 
                $alu = new alumno();
                $alu->name = $array[$i][0];
                $alu->fechaNacimiento = $array[$i][1];
                $alu->nivel = $array[$i][2];
                $alu->grado = $array[$i][3];
                $alu->grupo = $array[$i][4];
                $alu->save();
            }

            $cargaCompletaA = "<p style='color:green; float: right;'>Se han cargado exitosamente los alumnos a la base de datos<i class=material-icons>done_all</i></p>";
            $cargaCompletaT = "";
            $cargaCompletaP = "";
            $done = "";
            $doneDos = "";
            $sendTut = "";
            $sendProf = "";
            $accesTut = "";
            $accesProf = "";
        }else if ($request->get('tabla') == 'tutores') {
            for ($i=1; $i < sizeof($array); $i++) { 
                $tut = new tutore();
                $tut->name = $array[$i][0];
                $tut->email = $array[$i][1];

                $tutor = tutore::where('email', '=', $tut->email)->first();
                print_r($tutor);

                if ($tutor != NULL) {
                    $i = $i + 1;
                }else{
                    $tut->save();
                }
            }

            $cargaCompletaA = "";
            $cargaCompletaT = "<p style='color:green; float: right;'>Se han cargado exitosamente los tutores a la base de datos<i class=material-icons>done_all</i></p>";
            $cargaCompletaP = "";
            $done = "";
            $doneDos = "";
            $sendTut = "";
            $sendProf = "";
            $accesTut = "";
            $accesProf = "";
        }else if ($request->get('tabla') == 'profesor') {
            for ($i=1; $i < sizeof($array); $i++) { 
                $prof = new profesor();
                $prof->name = $array[$i][0];
                $prof->email = $array[$i][1];
                $prof->fechaNacimiento = $array[$i][2];
                $prof->nivel = $array[$i][3];
                $prof->grado = $array[$i][4];
                $prof->grupo = $array[$i][5];
                $prof->save();
            }

            $cargaCompletaA = "";
            $cargaCompletaT = "";
            $cargaCompletaP = "<p style='color:green; float: right;'>Se han cargado exitosamente los profesores a la base de datos<i class=material-icons>done_all</i></p>";
            $done = "";
            $doneDos = "";
            $sendTut = "";
            $sendProf = "";
            $accesTut = "";
            $accesProf = "";
        }
        return view('administrador/cargar_datos')->with(compact('cargaCompletaA', 'cargaCompletaT', 'cargaCompletaP', 'done', 'doneDos', 'sendTut', 'sendProf', 'accesTut', 'accesProf'));
    }
    public function documentos_admin(){

        $docu1 = DB::table('documentos')->where('titulo', '=', "Permisos de salida")->orderBy('id', 'desc')->limit(1)->get();

        $docu2 = DB::table('documentos')->where('titulo', '=', "Reglamento")->orderBy('id', 'desc')->limit(1)->get();

        $docu3 = DB::table('documentos')->where('titulo', '=', "Permisos fotos y videos")->orderBy('id', 'desc')->limit(1)->get();

        $docu4 = DB::table('documentos')->where('titulo', '=', "Normas de cobranza")->orderBy('id', 'desc')->limit(1)->get();

        $docu5 = DB::table('documentos')->where('titulo', '=', "Seguimiento a reuniones")->orderBy('id', 'desc')->limit(1)->get();

        $docu6 = DB::table('documentos')->where('titulo', '=', "Compras")->orderBy('id', 'desc')->limit(1)->get();
        
        $docu7 = DB::table('documentos')->where('titulo', '=', "Evaluaciones")->orderBy('id', 'desc')->limit(1)->get();

        $docu8 = DB::table('documentos')->where('titulo', '=', "Autoevaluaciones")->orderBy('id', 'desc')->limit(1)->get();
       
        if (count($docu1) < 1) {
            $temporal = (object) array('contenido' => '/subir_documentos/profesores');
            $docu1 = array($temporal);
        }if (count($docu2) < 1) {
            $temporal = (object) array('contenido' => '/subir_documentos/profesores');
            $docu2 = array($temporal);
        }if (count($docu3) < 1) {
            $temporal = (object) array('contenido' => '/subir_documentos/profesores');
            $docu3 = array($temporal);
        }if (count($docu4) < 1) {
            $temporal = (object) array('contenido' => '/subir_documentos/profesores');
            $docu4 = array($temporal);
        }if (count($docu5) < 1) {
            $temporal = (object) array('contenido' => '/subir_documentos/profesores');
            $docu5 = array($temporal);
        }if (count($docu6) < 1) {
            $temporal = (object) array('contenido' => '/subir_documentos/profesores');
            $docu6 = array($temporal);
        }if (count($docu7) < 1) {
            $temporal = (object) array('contenido' => '/subir_documentos/profesores');
            $docu7 = array($temporal);
        }if (count($docu8) < 1) {
            $temporal = (object) array('contenido' => '/subir_documentos/profesores');
            $docu8 = array($temporal);
        }
        return view('administrador/documentos_admin')->with(compact('docu1', 'docu2', 'docu3', 'docu4', 'docu5', 'docu6', 'docu7', 'docu8'));
    }
    public function subir_documentos($tipo){
        return view('administrador/subir_documentos')->with('tipo', $tipo);
    }
    public function guardar_documento(Request $request){
        $url_doc = $request->file('documento')->store('public/uploads/documentos');
        $sep = explode('/', $url_doc);
        $url_final = '/storage/'.$sep[1].'/'.$sep[2].'/'.$sep[3];

        $documento = new documentos;
        $documento['titulo'] = $request->get('titulo');
        $documento['contenido'] = $url_final;
        $documento['clasificacion'] = $request->get('tipo');
        $documento->save();
        return redirect('documentos_admin');
    }
    public function avisos_administrador(){
        $avisos = DB::table('avisos')->orderBy('id', 'desc')->get();
        return view('administrador/avisos_administrador')->with('avisos', $avisos);
    }
    public function eliminar_avisos()
    {
        $avisos = DB::table('avisos')->orderBy('id', 'desc')->get();
        return view('/administrador/eliminar_aviso')->with(compact('avisos'));
    }
    public function eliminar_contenido_aviso(Request $request){
        $aviso = DB::table('avisos')->where('id', '=', $request->get('idAviso'))->get();
        $av = public_path($aviso[0]->contenido);
        if (file_exists($av)) {
            unlink($av);
        }
        DB::table('avisos')->delete($request->get('idAviso'));
        return back();
    }
    public function guardar_avisos(Request $request){
        $url_aviso = $request->file('files')->store('public/uploads/avisos');
        $sep = explode('/', $url_aviso);
        $url_final = '/storage/'.$sep[1].'/'.$sep[2].'/'.$sep[3];

        $aviso = new avisos;
        $aviso['titulo'] = $request->get('titulo');
        $aviso['contenido'] = $url_final;
        $aviso->save();
        return redirect('avisos_admin');
    }
    public function guardar_calendario(Request $request)
    {

        print_r($request->all());

        $url_calendario = $request->file('files')->store('public/uploads/calendarios');
        $sep = explode('/', $url_calendario);
        $url_final = '/storage/'.$sep[1].'/'.$sep[2].'/'.$sep[3];

        $calendario = new Calendario;
        $calendario['usuario'] = $request->get('usuario');
        $calendario['mes'] = $request->get('mes');
        $calendario['anio'] = $request->get('anio');
        $calendario['contenido'] = $url_final;
        $calendario->save();
        return redirect('calendario_administrador');
    }
}
