<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RecoverPasswordController extends Controller
{
    public function olvidaste_contrasena(){
    	return view('/user_auth/recuperar_contrasena');
    }
    public function restaurar(){
    	return view('/user_auth/restaurar');
    }
}
