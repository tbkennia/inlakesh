<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\alumno;
use App\tutore;
use App\galeria;
use App\Calendario;
use App\MesCalendario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Storage;

class TutoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('Tutor');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calendario_anio = DB::table('calendario')->select('anio')->distinct()->where('usuario', "=", 2)->orderBy('anio', 'desc')->get();
        $calendario = DB::table('calendario')->where('usuario', "=", 2)->orderBy('anio', 'desc')->orderBy('mes', 'desc')->get();
        if (count($calendario)<1) {
            return redirect('calificaciones');
        }else{
            return view('tutores/calendario',compact('calendario','calendario_anio'));
        }
    }
    public function boletin()
    {
        $mesCalendario = MesCalendario::all();
        if (count($mesCalendario)<1) {
            return redirect('calendario');
        }else{
            $ulti  = mesCalendario::orderBy('id', 'desc')->first()->id;
            $ultimo = DB::table('mesBoletin')->where('id', "=", $ulti)->get();
            $mes = DB::table('mesBoletin')->where('id', "<", $ulti)->orderBy('id', 'desc')->get();
            $btn_activo = DB::table('boletines')->orderBy('id', 'desc')->where('idMesBoletin', '=', $ulti)->get();
            $btnes = DB::table('boletines')->orderBy('id', 'desc')->where('idMesBoletin', '!=', $ulti)->get();
            return view('/tutores/boletin')->with(compact('mes', 'ultimo', 'btn_activo', 'btnes'));
        }
    }
    public function calificaciones()
    {
        $tutor = DB::table('tutores')->where('id_user', auth()->user()->id)->get();
        $alumno = DB::table('alumnos')->where('tutor1', $tutor[0]->id)->get();
        if (sizeof($alumno) == 0) {
            $alumno = DB::table('alumnos')->where('tutor2', $tutor[0]->id)->get();
        }
        $url_boleta = Storage::url('uploads/boletas/primaria/primero/InlakeshBoleta_Primaria12017.pdf');
        return view('/tutores/calificaciones')->with(compact('alumno', 'url_boleta'));
    }
    public function galeria()
    {
        $galery = DB::table('galerias')->orderBy('id', 'desc')->get();
        
        return view('/tutores/galeria')->with('galery', $galery);
    }
    public function vista_galeria($id)
    {
        $vista_fotos = DB::table('detalle_galerias')->where('idgaleria', '=', $id)->get();
        $galery = DB::table('galerias')->where('id', '=', $id)->get();
        return view('/tutores/vista_galeria')->with(compact('id', 'vista_fotos', 'galery'));
    }
    public function documentos_tutor(){
        $docu1 = DB::table('documentos')->where('titulo', '=', "Permisos de salida")->orderBy('id', 'desc')->limit(1)->get();

        $docu2 = DB::table('documentos')->where('titulo', '=', "Reglamento")->orderBy('id', 'desc')->limit(1)->get();

        $docu3 = DB::table('documentos')->where('titulo', '=', "Permisos fotos y videos")->orderBy('id', 'desc')->limit(1)->get();

        $docu4 = DB::table('documentos')->where('titulo', '=', "Normas de cobranza")->orderBy('id', 'desc')->limit(1)->get();

        if (count($docu1) < 1) {
            $temporal = (object) array('contenido' => '#');
            $docu1 = array($temporal);
        }if (count($docu2) < 1) {
            $temporal = (object) array('contenido' => '#');
            $docu2 = array($temporal);
        }if (count($docu3) < 1) {
            $temporal = (object) array('contenido' => '#');
            $docu3 = array($temporal);
        }if (count($docu4) < 1) {
            $temporal = (object) array('contenido' => '#');
            $docu4 = array($temporal);
        }
        return view('tutores/documentos')->with(compact('docu1', 'docu2', 'docu3', 'docu4'));
    }
    public function avisos(){
        $avisos = DB::table('avisos')->orderBy('id', 'desc')->get();
        return view('tutores/avisos')->with('avisos', $avisos);
    }
    public function videos_tutores(){
        return view('/tutores/videos');
    }
}
