<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MesCalendario extends Model
{
    protected $table = 'mesBoletin';
    protected $primaryKey = 'id';
}
