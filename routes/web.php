<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

//Auth::routes();

		Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');

        // Registration Routes...
        if ($options['register'] ?? true) {
            Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
            Route::post('register', 'Auth\RegisterController@register');
        }

        // Password Reset Routes...
        if ($options['reset'] ?? true) {
            Route::resetPassword();
        }

        // Email Verification Routes...
        if ($options['verify'] ?? false) {
            Route::emailVerification();
        }

		Route::get('/home', 'HomeController@index')->name('home');

		Route::get('recuperar_contrasenia', 'RecoverPasswordController@olvidaste_contrasena')->name('recuperar_contrasenia');

		Route::get('restaurar', 'RecoverPasswordController@restaurar')->name('restaurar');

		//rutas tutores

		Route::group(['middleware' => 'Tutor'], function () {

			Route::get('/calendario', 'TutoreController@index')->name('calendario');

			Route::get('/boletin', 'TutoreController@boletin')->name('boletin');

			Route::get('/calificaciones', 'TutoreController@calificaciones')->name('calificaciones');
			
			Route::get('/galeria', 'TutoreController@galeria')->name('galeria');
			
			Route::get('/vista_galeria/{id}', 'TutoreController@vista_galeria')->name('vista_galeria');

			Route::get('/documentos_tutor', 'TutoreController@documentos_tutor')->name('documentos_tutor');
			
			Route::get('/avisos', 'TutoreController@avisos')->name('avisos');
			
			Route::get('/videos_tutores', 'TutoreController@videos_tutores')->name('videos_tutores');

		});

		//rutas administrador

		Route::group(['middleware' => 'Administrador'], function () {

			//resources

        	Route::resource('/calendario_admin','calendario');

        	//rutas get

			Route::get('/calendario_administrador', 'AdministradorController@index')->name('calendario_administrador');

			Route::get('/agregar_galeria', 'AdministradorController@agregar_galeria')->name('agregar_galeria');

			Route::get('/eliminar_galeria', 'AdministradorController@eliminar_galeria')->name('eliminar_galeria');

			Route::get('/alumnos', 'AdministradorController@alumnos')->name('alumnos');

			Route::get('/asignar_tutor/{id}', 'AdministradorController@asignar_tutor')->name('asignar_tutor');

			Route::get('/boletin_administrador', 'AdministradorController@boletin_administrador')->name('boletin_administrador');

			Route::get('/editar_galeria_administrador', 'AdministradorController@editar_galeria_administrador')->name('editar_galeria_administrador');

			Route::get('/galeria_administrador', 'AdministradorController@galeria_administrador')->name('galeria_administrador');

			Route::get('/lista/{grupo}', 'AdministradorController@lista')->name('lista');
			
			Route::get('/lista_vacia/', 'AdministradorController@lista_vacia')->name('lista_vacia');

			Route::get('/registrar_alumno', 'AdministradorController@registrar_alumno')->name('registrar_alumno');

			Route::get('/registrar_tutor/{id}', 'AdministradorController@registrar_tutor')->name('registrar_tutor');

			Route::get('/registrar_tutor2/{id}', 'AdministradorController@registrar_tutor2')->name('registrar_tutor2');

			Route::get('/tutores', 'AdministradorController@tutores')->name('tutores');

			Route::get('/vista_galeria_administrador/{id}', 'AdministradorController@vista_galeria_administrador')->name('vista_galeria_administrador');


			Route::get('/crear_calendario', 'AdministradorController@crear_calendario')->name('crear_calendario');

			Route::get('/editar_calendario', 'AdministradorController@editar_calendario')->name('editar_calendario');

			Route::get('/ver_calendario', 'AdministradorController@ver_calendario')->name('ver_calendario');

			Route::get('/crear_mes', 'AdministradorController@crear_mes')->name('crear_mes');

			Route::get('/eliminar_mes', 'AdministradorController@eliminar_mes')->name('eliminar_mes');

			Route::get('/profesores', 'AdministradorController@profesores')->name('profesores');

			Route::get('/registra_profesor', 'AdministradorController@registra_profesor')->name('registra_profesor');

			Route::get('/subir_db/{usuario}', 'AdministradorController@subir_db')->name('subir_db');

			Route::get('/cargar_datos', 'AdministradorController@cargar_datos')->name('cargar_datos');

			Route::get('/relacionar_hijo_tutor', 'AdministradorController@relacionar_hijo_tutor')->name('relacionar_hijo_tutor');

			Route::get('/documentos_admin', 'AdministradorController@documentos_admin')->name('documentos_admin');

			Route::get('/subir_documentos/{tipo}', 'AdministradorController@subir_documentos')->name('subir_documentos');

			Route::get('/editar_tutor/{id}', 'AdministradorController@editar_tutor')->name('editar_tutor');

			Route::get('editar_alumno/{id}', 'AdministradorController@editar_alumno')->name('editar_alumno');

			Route::get('/avisos_admin', 'AdministradorController@avisos_administrador')->name('avisos_admin');

			Route::get('/videos_administrador', 'AdministradorController@videos_administrador')->name('videos_administrador');

			Route::get('/eliminar_calendario', 'AdministradorController@eliminar_calendario')->name('eliminar_calendario');

			Route::get('/eliminar_avisos', 'AdministradorController@eliminar_avisos')->name('eliminar_avisos');

			//rutas post

			Route::post('crear_tutor', 'AdministradorController@crear_tutor');

	        Route::post('crear_alumno', 'AlumnoController@crear_alumno');

			Route::post('crear_profesor', 'AdministradorController@crear_profesor');

	        Route::post('tutor_asignado', 'AdministradorController@tutor_asignado');

	        Route::post('eliminar_tutor', 'AdministradorController@eliminar_tutor');

	        Route::post('eliminar_profesor', 'AdministradorController@eliminar_profesor');

	        Route::post('eliminar_alumno', 'AdministradorController@eliminar_alumno');

			Route::post('/actualiza_alumno', 'AdministradorController@actualiza_alumno');

			Route::post('editar_profesor', 'AdministradorController@editar_profesor');

			Route::post('/actualiza_tutor', 'AdministradorController@actualiza_tutor');

			Route::post('/actualiza_profesor', 'AdministradorController@actualiza_profesor');

			Route::post('/guardar_mes', 'AdministradorController@guardar_mes');

	        Route::post('eliminar_contenido_mes', 'AdministradorController@eliminar_contenido_mes');

	        Route::post('eliminar_contenido_galeria', 'AdministradorController@eliminar_contenido_galeria');

			Route::post('agregar_boletin/{id}', 'AdministradorController@agregar_boletin');

			Route::post('/guardar_boletin', 'AdministradorController@guardar_boletin');

			Route::post('guardar_galeria', 'AdministradorController@guardar_galeria');

			Route::post('guardar_fotos_galería', 'AdministradorController@guardar_fotos_galería');
			
			Route::post('/agregar_fotos_galeria/{id}', 'AdministradorController@agregar_fotos_galeria');

			Route::post('/generar_password', 'AdministradorController@generar_password');

			Route::post('/envio_mail_tutor', 'AdministradorController@envio_mail_tutor');

			Route::post('/envio_mail_un_tutor', 'AdministradorController@envio_mail_un_tutor');

			Route::post('/importar_db', 'AdministradorController@importar_db');

			Route::post('/dar_acceso', 'AdministradorController@dar_acceso');

			Route::post('/relacionar', 'AdministradorController@relacionar');

			Route::post('/envio_mail_profesor', 'AdministradorController@envio_mail_profesor');

			Route::post('/envio_mail_un_profesor', 'AdministradorController@envio_mail_un_profesor');

			Route::post('/guardar_documento', 'AdministradorController@guardar_documento');

			Route::post('/dejar_de_ser_tutor', 'AdministradorController@dejar_de_ser_tutor');

			Route::post('/asignar_tutelado', 'AdministradorController@asignar_tutelado');

			Route::post('/establecer_tutor', 'AdministradorController@establecer_tutor');

			Route::post('/guardar_avisos', 'AdministradorController@guardar_avisos');

			Route::post('/guardar_calendario', 'AdministradorController@guardar_calendario');

	        Route::post('eliminar_contenido_calendario', 'AdministradorController@eliminar_contenido_calendario');

	        Route::post('eliminar_contenido_aviso', 'AdministradorController@eliminar_contenido_aviso');

        });

		//rutas profesor

		Route::group(['middleware' => 'Profesor'], function () {

			Route::get('/calendario_profesor', 'Profesor@index')->name('calendario_profesor');

			Route::get('/alumnos_profesor', 'Profesor@alumnos_profesor')->name('alumnos_profesor');

			Route::get('/boletin_profesor', 'Profesor@boletin_profesor')->name('boletin_profesor');

			Route::get('/calificaciones_alumno/{id}', 'Profesor@calificaciones_alumno')->name('calificaciones_alumno');

			Route::get('/documentos_profesor', 'Profesor@documentos_profesor')->name('documentos_profesor');

			Route::get('/editar_calificaciones', 'Profesor@editar_calificaciones')->name('editar_calificaciones');

			Route::get('/galeria_profesor', 'Profesor@galeria_profesor')->name('galeria_profesor');

			Route::get('/vista_galeria_profesor/{id}', 'Profesor@vista_galeria_profesor')->name('vista_galeria_profesor');

			Route::get('/registrar_calificacion', 'Profesor@registrar_calificacion')->name('registrar_calificacion');
			
			Route::get('/avisos_profesor', 'Profesor@avisos_profesor')->name('avisos_profesor');

			Route::get('/videos_profesor', 'Profesor@videos_profesor')->name('videos_profesor');
		});

		

        Route::resource('/flipbook2','\rudrarajiv\flipbooklaravel\FlipBookController');