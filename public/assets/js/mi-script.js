function archivo(evt) {
  var files = evt.target.files; // FileList object

  // Obtenemos la imagen del campo "file".
  for (var i = 0, f; f = files[i]; i++) {
    //Solo admitimos imágenes.
    if (!f.type.match('image.*')) {
      continue;
    }
    var reader = new FileReader();
    reader.onload = (function(theFile) {
      return function(e) {
        // Insertamos la imagen
        document.getElementById("list").innerHTML = ['<img class="thumb sube-200 rounded img-border height-100" src="', e.target.result,'" title="', escape(theFile.name), ' "/>'].join('');
        
      };
    })(f);
    reader.readAsDataURL(f);
  }
}
document.getElementById('files').addEventListener('change', archivo, false);

function mostrar(id) {
  //console.log(id);
    if (id == "Jardín") {
      console.log("Jardín");
        $("#jardin").show();
        $("#primaria").hide();
        $("#secundaria").hide();
       // $("#grupo").show();
    }

    if (id == "Primaria") {
        $("#primaria").show();
        $("#jardin").hide();
        $("#secundaria").hide();
    }

    if (id == "Secundaria") {
        $("#jardin").hide();
        $("#primaria").hide();
        $("#secundaria").show();
    }
}
function mostrar2(id){
  if (id == "Capullo") {
    $("#grupo").show();
  }
  if (id == "Semilla") {
    $("#grupo").show();
  }
  if (id == "Bosque") {
    $("#grupo").show();
  }
  if (id == "Luna") {
    $("#grupo").show();
  }
  if (id == "1") {
    $("#grupo").show();
  }
  if (id == "2") {
    $("#grupo").show();
  }
  if (id == "3") {
    $("#grupo").show();
  }
  if (id == "4") {
    $("#grupo").show();
  }
  if (id == "5") {
    $("#grupo").show();
  }
  if (id == "6") {
    $("#grupo").show();
  }
  if (id == "7") {
    $("#grupo").show();
  }
  if (id == "8") {
    $("#grupo").show();
  }
  if (id == "9") {
    $("#grupo").show();
  }
}
function mas_tutelados(){
      $("#t2").show();
  
}
jQuery(document).ready(function($){
    $(document).ready(function() {
        $('.mi-selector').select2();
    });
});