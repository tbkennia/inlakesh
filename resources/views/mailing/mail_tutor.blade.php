<!DOCTYPE html>
<html>
<head>
  <title>Inlakesh</title>
  <style type="text/css">
    body{
      margin: 0px auto;
      padding: 0px;
    }
    main{
      height: 870px;
    }
    .centra{
      text-align: center;
      color: #5f5082;
    }
    .fs25{
      font-size: 25px;
    }
  </style>
</head>
<body>
  <main>
    <p class="centra fs25">Bienvenido al Colegio Inlakesh, los datos que aquí se te proporcionan son muy importantes, ya que con ellos podras ingresar a nuestro sistema y estar informado sobre avisos, calificaciones de tu(s) hijo(s) entre otras noticias. No compartas tus accesos con nadie..</p>
    <h3 class="centra">Datos personales de acceso al sistema Inlakesh</h3>
    <p class="centra"><b>Usuario:</b> {{$user}}</p>
    <p class="centra"><b>Contraseña:</b> {{$password}}</p>
    <p class="centra"><b>Matricula:</b> {{$matricula}}</p>
    <br><br>
  </main>
</body>
</html>