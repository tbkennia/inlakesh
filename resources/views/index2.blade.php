<!DOCTYPE html>
<html lang="en">
<!--================================================================================
DESARROLLADO POR NATOZ -> 11/2018   ñ.ñ
================================================================================ -->
  <head>
    <title>Inlakesh</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <!-- Favicons-->
    <link rel="icon" href="{{ asset('assets/images/favicon/favicon-32x32.png') }}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('assets/images/favicon/apple-touch-icon-152x152.png') }}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link href="{{ asset('assets/css/themes/collapsible-menu/materialize.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/collapsible-menu/style.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/collapsible-menu/mi-estilo.css') }}" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="{{ asset('assets/css/custom/custom.css" type="text/css') }}" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jvectormap/jquery-jvectormap.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/flag-icon/css/flag-icon.min.css') }}" type="text/css" rel="stylesheet">
  </head>
  <body>
    <!-- Visualizador de carga -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color gradient-45deg-purple-deep-orange gradient-shadow navega768">
          <div class="nav-wrapper">          
            <ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen">
                  <i class="material-icons">settings_overscan</i>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </header>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="fondo-main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav" class="nav-expanded nav-lock">
          <div class="brand-sidebar">
            <h1 class="logo-wrapper">
              <a href="/" class="brand-logo darken-1">
                <img src="{{ asset('assets/images/logo/materialize-logo.png') }}" alt="Inlakesh">
                <span class="logo-text hide-on-md-and-down"><img src="{{ asset('assets/images/logo/Inlakesh_logo.png') }}" alt="Inlakesh"></span>
              </a>
            </h1>
          </div>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- START CONTENT -->
          <!--start container-->
          <div class="color-inlakesh formulario">
            <div id="login-page" class="row pt-4 center">
              <div>
                <form class="login-form">
                  <div class="row margin">
                    <div class="input-field col s12">
                      <i class="material-icons prefix pt-5">person_outline</i>
                      <input id="username" type="text">
                      <label for="username" class="center-align">Username</label>
                    </div>
                  </div>
                  <div class="row margin">
                    <div class="input-field col s12">
                      <i class="material-icons prefix pt-5">lock_outline</i>
                      <input id="password" type="password">
                      <label for="password">Password</label>
                    </div>
                  </div>
                  <!--<div class="row">
                    <div class="col s12 m12 l12 ml-2 mt-3">
                      <input type="checkbox" id="remember-me" />
                      <label for="remember-me">Remember me</label>
                    </div>
                  </div>-->
                  <div class="row">
                    <div class="input-field col s12">
                      <a href="/calendario" class="btn waves-effect waves-light col s12">Login</a>
                    </div>
                  </div>
                  <!--<div class="row">
                    <div class="input-field col s6 m6 l6">
                      <p class="margin medium-small"><a href="https://pixinvent.com/materialize-material-design-admin-template/html/collapsible-menu/page-register.html">Register Now!</a></p>
                    </div>
                    <div class="input-field col s6 m6 l6">
                      <p class="margin right-align medium-small"><a href="https://pixinvent.com/materialize-material-design-admin-template/html/collapsible-menu/page-forgot-password.html">Forgot password ?</a></p>
                    </div>
                  </div>-->
                </form>
              </div>
            </div>
          </div>
        <!-- END CONTENT -->
        <!-- END RIGHT SIDEBAR NAV-->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START FOOTER -->
      <footer id="footer1" style="padding-left: 0px; padding-top: 8px; padding-bottom: 8px;" class="page-footer gradient-45deg-purple-deep-orange">
        <div class="footer-copyright">
          <div class="container">
            <span>Copyright ©
              <script type="text/javascript">
                document.write(new Date().getFullYear());
              </script> <a class="grey-text text-lighten-4" href="https://touchesbegan.com/" target="_blank">TOUCHESBEGAN</a> Todos los derechos reservados.</span>
            <span class="right hide-on-small-only"> Diseñado y desarrollado por <a class="grey-text text-lighten-4" href="https://touchesbegan.com/">TOUCHESBEGANMX</a></span>
          </div>
        </div>
      </footer>
      <!-- END FOOTER -->
      <!-- ================================================
    Scripts
    ================================================ -->
      <!-- jQuery Library -->
      <script type="text/javascript" src="{{ asset('assets/vendors/jquery-3.2.1.min.js') }}"></script>
      <!--materialize js-->
      <script type="text/javascript" src="{{ asset('assets/js/materialize.min.js') }}"></script>
      <!--prism-->
      <script type="text/javascript" src="{{ asset('assets/vendors/prism/prism.js') }}"></script>
      <!--scrollbar-->
      <script type="text/javascript" src="{{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
      <!-- chartjs -->
      <script type="text/javascript" src="{{ asset('assets/vendors/chartjs/chart.min.js') }}"></script>
      <!--plugins.js - Some Specific JS codes for Plugin Settings-->
      <script type="text/javascript" src="{{ asset('assets/js/plugins.js') }}"></script>
      <!--custom-script.js - Add your own theme custom JS-->
      <script type="text/javascript" src="{{ asset('assets/js/custom-script.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/js/scripts/dashboard-ecommerce.js') }}"></script>
  </body>
</html>
