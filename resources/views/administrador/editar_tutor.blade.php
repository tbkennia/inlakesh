<!DOCTYPE html>
<html lang="en">
<!--================================================================================
DESARROLLADO POR NATOZ -> 11/2018   ñ.ñ
================================================================================ -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Inlakesh - Editar padre</title>
    <!-- Favicons-->
    <link rel="icon" href="{{ asset('assets/images/favicon/favicon-32x32.png') }}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('assets/images/favicon/apple-touch-icon-152x152.png') }}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
    <link href="{{ asset('assets/css/themes/collapsible-menu/materialize.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/collapsible-menu/style.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/collapsible-menu/mi-estilo.css') }}" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="{{ asset('assets/css/custom/custom.css') }}" type="text/css" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/magnific-popup/magnific-popup.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/flag-icon/css/flag-icon.min.css') }}" type="text/css" rel="stylesheet">
  </head>
  <body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color fondo-lila">
          <div class="nav-wrapper">
            <ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen">
                  <i class="material-icons">settings_overscan</i>
                </a>
              </li>
            </ul>
            <!-- profile-dropdown -->
          </div>
        </nav>
      </div>
    </header>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <div id="fondo-gris" class="fondo-gris">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav" class="nav-lock nav-collapsible">
          <div class="brand-sidebar fondo-lila">
            <h1 class="logo-wrapper">
              <a href="{{ route('calendario_administrador') }}" class="brand-logo darken-1">
                <img src="{{ asset('assets/images/logo/materialize-logo.png') }}" alt="Inlakesh">
                <span class="logo-text hide-on-med-and-dow"><img src="{{ asset('assets/images/logo/Inlakesh_logo.png') }}" alt="Inlakesh"></span>
              </a>
              <a href="#" class="navbar-toggler">
                <i class="material-icons">radio_button_checked</i>
              </a>
            </h1>
          </div>
          <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container ps-active-y fondo-morado" style="transform: translateX(-100%);">
            <li class="no-padding">
              <ul class="collapsible" data-collapsible="accordion">
                <li class="bold">
                  <a class="collapsibl-header waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">person_outline</i>
                    <span class="nav-text texto-amarillo">{{ Auth::user()->name }}</span>
                  </a>
                </li>
                <li class="bold pl-8 pr-8">
                <div class="divider"></div>
                </li>
                <li class="bold">
                  <a href="{{ route('calendario_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">today</i>
                    <span class="nav-text texto-amarillo">Calendario</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('boletin_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">assignment</i>
                    <span class="nav-text texto-amarillo">Boletin</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('galeria_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">perm_media</i>
                    <span class="nav-text texto-amarillo">Galería</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('videos_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">movie</i>
                    <span class="nav-text texto-amarillo">Videos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('alumnos') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">group_outline</i>
                    <span class="nav-text texto-amarillo">Alumnos</span>
                  </a>
                </li>
                <li class="bold active">
                  <a href="{{ route('tutores') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">group_outline</i>
                    <span class="nav-text texto-amarillo">Padres</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('profesores') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">group_outline</i>
                    <span class="nav-text texto-amarillo">Maestros</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('cargar_datos') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">archive</i>
                    <span class="nav-text texto-amarillo">Cargar datos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('documentos_admin') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">library_books</i>
                    <span class="nav-text texto-amarillo">Documentos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('avisos_admin') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">description</i>
                    <span class="nav-text texto-amarillo">Avisos</span>
                  </a>
                </li>
                <li class="bold pl-8 pr-8">
                <div class="divider"></div>
                </li>
                <li class="bold">
                  <a class="dropdow-item waves-effect waves-cyan texto-amarillo" href="{{ route('logout') }}"
                     onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      <i class="material-icons texto-amarillo">keyboard_tab</i>
                     <span class="nav-text texto-amarillo">{{ __('Cerrar sesión') }}</span>
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </li>
              </ul>
            </li>
          <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 609px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 293px;"></div></div></ul>
          <a href="advance-ui-transitions#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only gradient-45deg-light-blue-cyan gradient-shadow">
            <i class="material-icons">menu</i>
          </a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content">
          <!--start container-->
          <div class="container">
            <div class="section">
            <h4 class="texto-gris fuente-waldorf">Editar padre</h4>
              <div class="divider"></div>
              <div class="section">
                <div class="row">
                  <div class="col s12">
                    <div class="formulario">
                      <div>
                        <h5 class="texto-gris fuente-waldorf">Información general padre:</h5>
                        <p>N° de registro Web: <?php echo $tutor->id;?></p>
                        <p>Padre: <?php echo $tutor->name;?></p>
                        <p>Correo: <?php echo $tutor->email;?></p>
                      </div>
                      <div class="divider"></div>
                      <div class="divider"></div>
                      <div>
                        <table>
                          <thead>
                            <tr>
                              <th class="center" colspan="5">SOY PRIMER PADRE DE(L LOS) ALUMNO(S)</th>
                            </tr>
                            <tr>
                              <th>N° de<br>registro Web</th>
                              <th>Tutelado</th>
                              <th>Fecha de nacimiento</th>
                              <th>Nivel</th>
                              <th>Dejar de<br>ser padre</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($tutelado1 as $t1) { ?>
                            <tr>
                                <td><?php print_r($t1->id); ?></td>
                                <td><?php print_r($t1->name); ?></td>
                                <td><?php print_r($t1->fechaNacimiento); ?></td>
                                <td><?php print_r($t1->nivel); ?></td>
                                <td>
                                  <form action="{{ url('/dejar_de_ser_tutor') }}" method="post">
                                    @csrf
                                    <input type="text" name="vista" value="1" hidden>
                                    <input type="text" name="tutelado" id="tutelado" value="<?php echo $t1->id;?>" hidden>
                                    <input type="text" name="tutor" id="tutor" value="<?php echo $tutor->id; ?>" hidden>
                                    <input type="image" name="imageField" src="{{ asset('assets/images/icon/borrar.png') }}" />
                                  </form>
                                </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>

                        <table>
                          <thead>
                            <tr>
                              <th class="center" colspan="5">SOY SEGUNDO PADRE DE(L LOS) ALUMNO(S)</th>
                            </tr>
                            <tr>
                              <th>N° de<br>registro Web</th>
                              <th>Tutelado</th>
                              <th>Fecha de nacimiento</th>
                              <th>Nivel</th>
                              <th>Dejar de<br>ser padre</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($tutelado2 as $t2) { ?>
                            <tr>
                                <td><?php print_r($t2->id); ?></td>
                                <td><?php print_r($t2->name); ?></td>
                                <td><?php print_r($t2->fechaNacimiento); ?></td>
                                <td><?php print_r($t2->nivel); ?></td>
                                <td>
                                  <form action="{{ url('/dejar_de_ser_tutor') }}" method="post">
                                    @csrf
                                    <input type="text" name="vista" value="1" hidden>
                                    <input type="text" name="tutelado" id="tutelado" value="<?php echo $t2->id;?>" hidden>
                                    <input type="text" name="tutor" id="tutor" value="<?php echo $tutor->id; ?>" hidden>
                                    <input type="image" name="imageField" src="{{ asset('assets/images/icon/borrar.png') }}" />
                                  </form>
                                </td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                      <br>
                      <div class="divider"></div>
                      <div class="divider"></div>
                      <div>
                        <h5 class="texto-gris fuente-waldorf">Agregar más tutelados</h5>
                        <p>1.- Puedes registar un nuevo tutelado</p>
                        <p>
                          <a href="registrar_alumno">
                            <button class="btn fondo-lila texto-amarillo">Registrar alumno</button>
                          </a>
                        </p>
                        <br>
                        <div class="divider"></div>
                        <br>
                        <p>2.- Ó puedes asignarle un tutelado que ya este dado de alta en el colegio</p>
                        <form action="{{ url('/asignar_tutelado') }}" method="post">
                          @csrf
                          <p></p>
                          <div class="ml-10">
                            <input type="text" name="tutor" value="<?php echo $tutor->id; ?>" hidden>
                            <select required="required" class="browser-default selectpicker" name="nivel_tutor">
                              <option disabled selected>Selecciona que nivel de padre sera </option>
                              <option value="1">Primer padre</option>
                              <option value="2">Segundo padre</option>
                            </select>
                            <p class="text_red">OBLIGATORIO *</p>
                            <br>
                            <select required="required" class="browser-default selectpicker" data-show-subtext="true" data-live-search="true" name="alumno" id="alumno" style="height: 50px; overflow-y: scroll;" >
                              <option value="" disabled selected>Selecciona alumno</option>
                              <?php foreach ($alumno as $alu) {?>
                                <option value=<?php  echo $alu->id; ?>>
                                  <?php  echo $alu->name; ?>
                                </option>
                              <?php } ?>        
                            </select>
                            <p class="text_red">OBLIGATORIO *</p>
                          </div>
                          <input type="submit" class="btn ml-1 mt-2" name="" value="Asignar">
                        </form>
                        <br>
                      </div>
                      <div id="login-page" class="row pt-1 center">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--end container-->
          </div>
        </section>
        <!-- END CONTENT -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END WRAPPER -->
      </div>
    <!-- END MAIN -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
    <footer id="footer1" class="fondo-amarillo texto-gris-oscuro pt-1">
      <div class="footer-copyright">
        <div class="container">
          <span>Copyright ©
            <script type="text/javascript">
              document.write(new Date().getFullYear());
            </script> <a href="https://touchesbegan.com/" target="_blank">TOUCHESBEGAN</a> Todos los derechos reservados.</span>
          <span class="right hide-on-small-only"> Diseñado y desarrollado por <a href="https://touchesbegan.com/">TOUCHESBEGANMX</a></span>
        </div>
      </div>
    </footer>
    <!-- END FOOTER -->
    <!-- ================================================
    Scripts
    ================================================ -->
    <!-- jQuery Library -->
    <script type="text/javascript" src="{{ asset('assets/vendors/jquery-3.2.1.min.js') }}"></script>
    <!--materialize js-->
    <script type="text/javascript" src="{{ asset('assets/js/materialize.min.js') }}"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="{{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <!-- masonry -->
    <script src="{{ asset('assets/vendors/masonry.pkgd.min.js') }}"></script>
    <!-- imagesloaded -->
    <script src="{{ asset('assets/vendors/imagesloaded.pkgd.min.js') }}"></script>
    <!-- magnific-popup -->
    <script type="text/javascript" src="{{ asset('assets/vendors/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="{{ asset('assets/js/plugins.js') }}"></script>
    <!--media-gallary-page.js - Page specific JS-->
    <script type="text/javascript" src="{{ asset('assets/js/scripts/media-gallary-page.js') }}"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="{{ asset('assets/js/custom-script.js') }}"></script>
    <!--mi-script.js - Page specific JS-->
    <script type="text/javascript" src="{{ asset('assets/js/mi-script.js') }}"></script>
  </body>
</html>
