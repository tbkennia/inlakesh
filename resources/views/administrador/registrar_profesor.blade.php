<!DOCTYPE html>
<html lang="en">
<!--================================================================================
DESARROLLADO POR NATOZ -> 11/2018   ñ.ñ
================================================================================ -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Inlakesh - Registrar maestro</title>
    <!-- Favicons-->
    <link rel="icon" href="{{ asset('assets/images/favicon/favicon-32x32.png') }}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('assets/images/favicon/apple-touch-icon-152x152.png') }}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link href="{{ asset('assets/css/themes/collapsible-menu/materialize.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/collapsible-menu/style.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/collapsible-menu/mi-estilo.css') }}" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="{{ asset('assets/css/custom/custom.css') }}" type="text/css" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/magnific-popup/magnific-popup.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/flag-icon/css/flag-icon.min.css') }}" type="text/css" rel="stylesheet">
  </head>
  <body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color fondo-lila">
          <div class="nav-wrapper">
            <ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen">
                  <i class="material-icons">settings_overscan</i>
                </a>
              </li>
            </ul>
            <!-- profile-dropdown -->
          </div>
        </nav>
      </div>
    </header>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <div id="fondo-gris" class="fondo-gris">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav" class="nav-lock nav-collapsible">
          <div class="brand-sidebar fondo-lila">
            <h1 class="logo-wrapper">
              <a href="{{ route('calendario_administrador') }}" class="brand-logo darken-1">
                <img src="{{ asset('assets/images/logo/materialize-logo.png') }}" alt="Inlakesh">
                <span class="logo-text hide-on-med-and-dow"><img src="{{ asset('assets/images/logo/Inlakesh_logo.png') }}" alt="Inlakesh"></span>
              </a>
              <a href="#" class="navbar-toggler">
                <i class="material-icons">radio_button_checked</i>
              </a>
            </h1>
          </div>
          <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container ps-active-y fondo-morado" style="transform: translateX(-100%);">
            <li class="no-padding">
              <ul class="collapsible" data-collapsible="accordion">
                <li class="bold">
                  <a class="collapsibl-header waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">person_outline</i>
                    <span class="nav-text texto-amarillo">{{ Auth::user()->name }}</span>
                  </a>
                </li>
                <li class="bold pl-8 pr-8">
                <div class="divider"></div>
                </li>
                <li class="bold">
                  <a href="{{ route('calendario_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">today</i>
                    <span class="nav-text texto-amarillo">Calendario</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('boletin_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">assignment</i>
                    <span class="nav-text texto-amarillo">Boletin</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('galeria_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">perm_media</i>
                    <span class="nav-text texto-amarillo">Galería</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('videos_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">movie</i>
                    <span class="nav-text texto-amarillo">Videos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('alumnos') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">group_outline</i>
                    <span class="nav-text texto-amarillo">Alumnos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('tutores') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">group_outline</i>
                    <span class="nav-text texto-amarillo">Padres</span>
                  </a>
                </li>
                <li class="bold active">
                  <a href="{{ route('profesores') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">group_outline</i>
                    <span class="nav-text texto-amarillo">Maestros</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('cargar_datos') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">archive</i>
                    <span class="nav-text texto-amarillo">Cargar datos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('documentos_admin') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">library_books</i>
                    <span class="nav-text texto-amarillo">Documentos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('avisos_admin') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">description</i>
                    <span class="nav-text texto-amarillo">Avisos</span>
                  </a>
                </li>
                <li class="bold pl-8 pr-8">
                <div class="divider"></div>
                </li>
                <li class="bold">
                  <a class="dropdow-item waves-effect waves-cyan texto-amarillo" href="{{ route('logout') }}"
                     onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      <i class="material-icons texto-amarillo">keyboard_tab</i>
                     <span class="nav-text texto-amarillo">{{ __('Cerrar sesión') }}</span>
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </li>
              </ul>
            </li>
          <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 609px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 293px;"></div></div></ul>
          <a href="advance-ui-transitions#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only gradient-45deg-light-blue-cyan gradient-shadow">
            <i class="material-icons">menu</i>
          </a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content">
          <!--start container-->
          <div class="container">
            <div class="section">
            <h4 class="texto-gris fuente-waldorf">Registrar un maestro nuevo</h4>
              <div class="divider"></div>
              <div class="section">
                <div class="row">
                  <div class="col s12">
                    <div class="texto-gris formulario">
                      <div id="login-page" class="row pt-1 center">
                        <div>
                          <form class="login-form" action="{{ url('/crear_profesor') }}" method="post">
                            @csrf
                            <div class="row margin">
                              <div class="input-field col s12">
                                <i class="material-icons prefix pt-5 texto-gris">person_outline</i>
                                <input id="name" name="name" type="text" required="">
                                <label for="name" class="center-align texto-gris">Nombre completo</label>
                              </div>
                            </div>
                            <div class="row margin">
                              <div class="input-field col s12">
                                <i class="material-icons prefix pt-5 texto-gris">person_outline</i>
                                <input id="email"  class="form-control @error('email') is-invalid @enderror"  name="email" type="email" value="{{ old('email') }}" required="" autocomplete="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label for="username" class="center-align texto-gris">Correo</label>
                              </div>
                            </div>
                            <?php $pass = rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9).rand(0, 9); ?>
                            
                            <div class="row margin" hidden="true">
                              <div class="input-field col s12">
                                <i class="material-icons prefix pt-5 texto-gris">lock_outline</i>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="<?php print_r($pass); ?>" required autocomplete="new-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <label for="password texto-gris">Contraseña</label>
                              </div>
                            </div>
                            <div class="row margin" hidden="true">
                              <div class="input-field col s12">
                                <i class="material-icons prefix pt-5">lock_outline</i>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" value="<?php print_r($pass); ?>" required autocomplete="new-password">
                                <label for="password">Confirmar Contraseña</label>
                              </div>
                            </div>
                            <div class="row margin">
                              <div class="input-field col s12">
                                <i class="material-icons prefix pt-5">replay_5</i>
                                <input id="edad" name="edad" type="date" min="3" max="18" required="">
                                <label for="edad" class="center-align mtn-30 texto-gris">Fecha de nacimiento</label>
                              </div>
                            </div>
                            <div class="row margin pt-3">
                              <div class="input-field col s0">
                                <i class="material-icons prefix mrn-50 pt-5">school</i>
                              </div>
                              <div class="input-field col s12">
                                <i class="prefix"></i>
                                <select required="" class="mtn-30" id="grado_nivel" name="grado_nivel" onChange="mostrar(this.value);">
                                  <option selected="true" disabled="disabled">Sección</option>
                                  <option value="Jardín">Jardín</option>
                                  <option value="Primaria">Primaria</option>
                                  <option value="Secundaria">Secundaria</option>
                                </select>
                              </div>
                            </div>
                            <div class="row margin pt-3" id="jardin" hidden="">
                              <div class="input-field col s0">
                                <i class="material-icons prefix mrn-50 pt-5">people_outline</i>
                              </div>
                              <div class="input-field col s12">
                                <i class="prefix"></i>
                                <select class="mtn-30" id="tres" name="grado" onChange="mostrar2(this.value);">
                                  <option selected="true" disabled="disabled">Grado</option>
                                  <option value="Capullo">Capullo</option>
                                  <option value="Semilla">Semilla</option>
                                  <option value="Bosque">Bosque</option>
                                  <option value="Luna">Luna</option>
                                </select>
                              </div>
                            </div>
                            <div class="row margin pt-3" id="primaria" hidden="">
                              <div class="input-field col s0">
                                <i class="material-icons prefix mrn-50 pt-5">people_outline</i>
                              </div>
                              <div class="input-field col s12">
                                <i class="prefix"></i>
                                <select required="" class="mtn-30" id="prima" name="grado" onChange="mostrar2(this.value);">
                                  <option selected="true" disabled="disabled">Grado</option>
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                                </select>
                              </div>
                            </div>
                            <div class="row margin pt-3" id="secundaria" hidden="">
                              <div class="input-field col s0">
                                <i class="material-icons prefix mrn-50 pt-5">people_outline</i>
                              </div>
                              <div class="input-field col s12">
                                <i class="prefix"></i>
                                <select class="mtn-30" id="tres" name="grado" onChange="mostrar2(this.value);">
                                  <option selected="true" disabled="disabled">Grado</option>
                                  <option value="6">6</option>
                                  <option value="7">7</option>
                                  <option value="8">8</option>
                                  <option value="9">9</option>
                                </select>
                              </div>
                            </div>
                            <div class="row margin pt-3" id="grupo" hidden="">
                              <div class="input-field col s0">
                                <i class="material-icons prefix mrn-50 pt-5">people_outline</i>
                              </div>
                              <div class="input-field col s12">
                                <i class="prefix"></i>
                                <select class="mtn-30" name="grupo">
                                  <option selected="true" disabled="disabled">Grupo</option>
                                  <option value="A">A</option>
                                  <option value="B" disabled="disabled">B</option>
                                  <option value="C" disabled="disabled">C</option>
                                  <option value="D" disabled="disabled">D</option>
                                </select>
                              </div>
                            </div>
                            <div class="row">
                              <div class="input-field col s12">
                                <input type="submit" name="asignar" value="{{ __('Registra') }}" class="btn fondo-lila texto-amarillo col s12">
                                <!--<a href="asignar_tutor" class="btn waves-effect waves-light col s12">Asignar tutor existente</a>-->
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--end container-->
          </div>
        </section>
        <!-- END CONTENT -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END WRAPPER -->
      </div>
    <!-- END MAIN -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
    <footer id="footer1" class="fondo-amarillo texto-gris-oscuro pt-1">
      <div class="footer-copyright">
        <div class="container">
          <span>Copyright ©
            <script type="text/javascript">
              document.write(new Date().getFullYear());
            </script> <a href="https://touchesbegan.com/" target="_blank">TOUCHESBEGAN</a> Todos los derechos reservados.</span>
          <span class="right hide-on-small-only"> Diseñado y desarrollado por <a href="https://touchesbegan.com/">TOUCHESBEGANMX</a></span>
        </div>
      </div>
    </footer>
    <!-- END FOOTER -->
    <!-- ================================================
    Scripts
    ================================================ -->
    <!-- jQuery Library -->
    <script type="text/javascript" src="{{ asset('assets/vendors/jquery-3.2.1.min.js') }}"></script>
    <!--materialize js-->
    <script type="text/javascript" src="{{ asset('assets/js/materialize.min.js') }}"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="{{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <!-- masonry -->
    <script src="{{ asset('assets/vendors/masonry.pkgd.min.js') }}"></script>
    <!-- imagesloaded -->
    <script src="{{ asset('assets/vendors/imagesloaded.pkgd.min.js') }}"></script>
    <!-- magnific-popup -->
    <script type="text/javascript" src="{{ asset('assets/vendors/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="{{ asset('assets/js/plugins.js') }}"></script>
    <!--media-gallary-page.js - Page specific JS-->
    <script type="text/javascript" src="{{ asset('assets/js/scripts/media-gallary-page.js') }}"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="{{ asset('assets/js/custom-script.js') }}"></script>
    <!--mi-script.js - Page specific JS-->
    <script type="text/javascript" src="{{ asset('assets/js/mi-script.js') }}"></script>
  </body>
</html>
