<!DOCTYPE html>
<html lang="en">
<!--================================================================================
DESARROLLADO POR NATOZ -> 11/2018   ñ.ñ
================================================================================ -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Inlakesh - Editar alumno</title>
    <!-- Favicons-->
    <link rel="icon" href="{{ asset('assets/images/favicon/favicon-32x32.png') }}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('assets/images/favicon/apple-touch-icon-152x152.png') }}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css'>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>

    <link href="{{ asset('assets/css/themes/collapsible-menu/materialize.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/collapsible-menu/style.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/collapsible-menu/mi-estilo.css') }}" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="{{ asset('assets/css/custom/custom.css') }}" type="text/css" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/magnific-popup/magnific-popup.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/flag-icon/css/flag-icon.min.css') }}" type="text/css" rel="stylesheet">
  </head>
  <body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color fondo-lila">
          <div class="nav-wrapper">
            <ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen">
                  <i class="material-icons">settings_overscan</i>
                </a>
              </li>
            </ul>
            <!-- profile-dropdown -->
          </div>
        </nav>
      </div>
    </header>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <div id="fondo-gris" class="fondo-gris">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav" class="nav-lock nav-collapsible">
          <div class="brand-sidebar fondo-lila">
            <h1 class="logo-wrapper">
              <a href="{{ route('calendario_administrador') }}" class="brand-logo darken-1">
                <img src="{{ asset('assets/images/logo/materialize-logo.png') }}" alt="Inlakesh">
                <span class="logo-text hide-on-med-and-dow"><img src="{{ asset('assets/images/logo/Inlakesh_logo.png') }}" alt="Inlakesh"></span>
              </a>
              <a href="#" class="navbar-toggler">
                <i class="material-icons">radio_button_checked</i>
              </a>
            </h1>
          </div>
          <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container ps-active-y fondo-morado" style="transform: translateX(-100%);">
            <li class="no-padding">
              <ul class="collapsible" data-collapsible="accordion">
                <li class="bold">
                  <a class="collapsibl-header waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">person_outline</i>
                    <span class="nav-text texto-amarillo">{{ Auth::user()->name }}</span>
                  </a>
                </li>
                <li class="bold pl-8 pr-8">
                <div class="divider"></div>
                </li>
                <li class="bold">
                  <a href="{{ route('calendario_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">today</i>
                    <span class="nav-text texto-amarillo">Calendario</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('boletin_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">assignment</i>
                    <span class="nav-text texto-amarillo">Boletin</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('galeria_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">perm_media</i>
                    <span class="nav-text texto-amarillo">Galería</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('videos_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">movie</i>
                    <span class="nav-text texto-amarillo">Videos</span>
                  </a>
                </li>
                <li class="bold active">
                  <a href="{{ route('alumnos') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">group_outline</i>
                    <span class="nav-text texto-amarillo">Alumnos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('tutores') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">group_outline</i>
                    <span class="nav-text texto-amarillo">Padres</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('profesores') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">group_outline</i>
                    <span class="nav-text texto-amarillo">Maestros</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('cargar_datos') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">archive</i>
                    <span class="nav-text texto-amarillo">Cargar datos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('documentos_admin') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">library_books</i>
                    <span class="nav-text texto-amarillo">Documentos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('avisos_admin') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">description</i>
                    <span class="nav-text texto-amarillo">Avisos</span>
                  </a>
                </li>
                <li class="bold pl-8 pr-8">
                <div class="divider"></div>
                </li>
                <li class="bold">
                  <a class="dropdow-item waves-effect waves-cyan texto-amarillo" href="{{ route('logout') }}"
                     onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      <i class="material-icons texto-amarillo">keyboard_tab</i>
                     <span class="nav-text texto-amarillo">{{ __('Cerrar sesión') }}</span>
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </li>
              </ul>
            </li>
          <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 609px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 293px;"></div></div></ul>
          <a href="advance-ui-transitions#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only gradient-45deg-light-blue-cyan gradient-shadow">
            <i class="material-icons">menu</i>
          </a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content">
          <!--start container-->
          <div class="container">
            <div class="section">
            <h4 class="texto-gris fuente-waldorf">Editar alumno</h4>
              <div class="divider"></div>
              <div class="section">
                <div class="row">
                  <div class="col s12">
                    <div class="formulario">
                      <div>
                        <table>
                          <thead>
                            <tr>
                              <th class="center" colspan="5">Padre(s):</th>
                            </tr>
                            <tr>
                              <th>N° de<br>registro Web</th>
                              <th>Padre</th>
                              <th>Correo</th>
                              <th>Dejar de<br>ser padre</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><?php print_r($tutor1_id) ?></td>
                              <td><?php print_r($tutor1_name) ?></td>
                              <td><?php print_r($tutor1_mail) ?></td>
                              <td>
                                <form action="{{ url('/dejar_de_ser_tutor') }}" method="post">
                                    @csrf
                                    <input type="text" name="vista" value="2" hidden>
                                    <input type="text" name="tutelado" id="tutelado" value="<?php echo $alumno->id;?>" hidden>
                                    <input type="text" name="tutor" id="tutor1" value="<?php echo $tutor1_id; ?>" hidden>
                                    <input type="image" name="imageField" id="uno" src="{{ asset('assets/images/icon/borrar.png') }}" />
                                  </form>
                              </td>
                            </tr>
                            <tr>
                              <td><?php print_r($tutor2_id) ?></td>
                              <td><?php print_r($tutor2_name) ?></td>
                              <td><?php print_r($tutor2_mail) ?></td>
                              <td>
                                <form action="{{ url('/dejar_de_ser_tutor') }}" method="post">
                                    @csrf
                                    <input type="text" name="vista" value="2" hidden>
                                    <input type="text" name="tutelado" id="tutelado" value="<?php echo $alumno->id;?>" hidden>
                                    <input type="text" name="tutor" id="tutor2" value="<?php echo $tutor2_id; ?>" hidden>
                                    <input type="image" name="imageField" id="dos" src="{{ asset('assets/images/icon/borrar.png') }}" />
                                  </form>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="divider"></div>
                      <div class="divider"></div>
                      <div>
                        <br>
                        <h6 class="">Agregar un padre</h6>
                        <p class="">1.- Deseas registrar un nuevo padre para este alumno</p>
                        <p class="">- Que nivel de padre sera:</p>
                        <?php if ($tutor1_id != "") { ?>
                        <a href="{{ route('registrar_tutor',$alumno->id) }}" class="btn fondo-lila texto-amarillo mb-2" disabled>Añadir padre principal</a>
                        <?php } elseif ( $tutor1_id == "") { ?>
                        <a href="{{ route('registrar_tutor',$alumno->id) }}" class="btn fondo-lila texto-amarillo mb-2">Añadir padre principal</a>
                        <?php } ?>
                        <?php if ($tutor2_id != "") { ?>
                        <a href="{{ route('registrar_tutor2',$alumno->id) }}" class="btn fondo-lila texto-amarillo mb-2" disabled>Añadir segundo padre</a>
                        <?php } elseif ( $tutor2_id == "") { ?>
                        <a href="{{ route('registrar_tutor2',$alumno->id) }}" class="btn fondo-lila texto-amarillo mb-2">Añadir segundo padre</a>
                        <?php } ?>
                        <p class="">2.- Deseas asignar un padre que ya existe para este alumno</p>
                        


                        <?php print_r($yaExiste) ?>
                        <form action="{{ url('/establecer_tutor') }}" method="post">
                          @csrf
                          <div class="ml-10">
                            <input type="text" name="alumno" value="<?php echo $alumno->id; ?>" hidden>
                            <select required="required" class="browser-default selectpicker" name="nivel_tutor">
                              <option disabled selected>Selecciona que nivel de padre sera </option>
                              <option value="1">Primer padre</option>
                              <option value="2">Segundo padre</option>
                            </select>
                            <p class="text_red">OBLIGATORIO *</p>
                            <br>
                            <select required="required" class="browser-default selectpicker" data-show-subtext="true" data-live-search="true" name="tutor_existente" id="tutor_existente" style="height: 50px; overflow-y: scroll;" >
                              <option value="" disabled selected>Selecciona padre</option>
                              <?php foreach ($tutor as $tut) {?>
                                <option value=<?php  echo $tut->id; ?>>
                                  <?php  echo $tut->name; ?>
                                </option>
                              <?php } ?>        
                            </select>
                            <p class="text_red">OBLIGATORIO *</p>
                          </div>
                          <input type="submit" class="btn fondo-lila texto-amarillo ml-1 mt-2" name="" value="Asignar">
                        </form>




                      </div>
                      <div class="divider"></div>
                      <div class="divider"></div>
                      <div id="login-page" class="row pt-1 center">
                        <div>
                        <h5 class="texto-gris fuente-waldorf">Datos del alumno(a):</h5>
                          <form class="login-form" action="{{ url('/actualiza_alumno') }}" method="POST">
                            @csrf
                            <div class="row margin">
                              <div class="input-field col s12">
                                <i class="material-icons prefix pt-5">person_outline</i>
                                <input id="alumno" name="alumno" type="text" value="<?php echo $alumno->id;?>" hidden>
                                <input type="text" value="<?php echo $alumno->id;?>" disabled>
                                <label for="username" class="center-align texto-gris fuente-waldorf">N° de registro Web</label>
                              </div>
                            </div>
                            <div class="row margin">
                              <div class="input-field col s12">
                                <i class="material-icons prefix pt-5">person_outline</i>
                                <input id="name" name="name" type="text" value="<?php echo $alumno->name;?>">
                                <label for="username" class="center-align texto-gris fuente-waldorf">Nombre</label>
                              </div>
                            </div>
                            <div class="row margin">
                              <div class="input-field col s12">
                                <i class="material-icons prefix pt-5">replay_5</i>
                                <input id="fechaNacimiento" name="fechaNacimiento" type="date" value="<?php echo $alumno->fechaNacimiento;?>" required>
                                <label for="username" class="center-align mtn-30 texto-gris fuente-waldorf">Fecha de nacimiento</label>
                              </div>
                            </div>

                            
                            <?php
                              $var = $alumno->nivel;
                              //print_r($var);
                              if($var == "Jardín"){
                                  $sU = "selected";
                                  $sD = "";
                                  $sB = "";
                                  $s4 = 'disabled';
                                  $s5 = 'disabled';
                                  $s6 = 'disabled';
                              }else if($var == "Primaria"){
                                  $sU = "";
                                  $sD = "selected";
                                  $sB = "";
                                  $s4 = "";
                                  $s5 = "";
                                  $s6 = "";
                              }else if($var == "Secundaria"){
                                  $sU = "";
                                  $sD = "";
                                  $sB = "selected";
                                  $s4 = 'disabled';
                                  $s5 = 'disabled';
                                  $s6 = 'disabled';
                              }
                            ?>
                            <div class="row margin pt-3">
                              <div class="input-field col s0">
                                <i class="material-icons prefix mrn-50 pt-5">school</i>
                              </div>
                              <div class="input-field col s12">
                                <i class="prefix"></i>
                                <select class="mtn-30" id="nivelgdo" name="nivelgdo" onChange="mostrar(this.value);">
                                  <option selected="true" disabled="disabled">Sección</option>
                                  <option value="Jardín"<?php echo $sU;?>>Jardín</option>
                                  <option value="Primaria"<?php echo $sD;?>>Primaria</option>
                                  <option value="Secundaria"<?php echo $sB;?>>Secundaria</option>
                                </select>
                              </div>
                            </div>
                            
                            <?php
                              $var = $alumno->grado;
                              //print_r($var);
                              if($var == "Capullo"){
                                  $sCap = "selected";
                                  $sSem = "";
                                  $sBos = "";
                                  $sLun = "";
                                  $s1 = "";
                                  $s2 = "";
                                  $s3 = "";
                                  $s4 = "";
                                  $s5 = "";
                                  $s6 = "";
                                  $s7 = "";
                                  $s8 = "";
                                  $s9 = "";
                              }else if($var == "Semilla"){
                                  $sCap = "";
                                  $sSem = "selected";
                                  $sBos = "";
                                  $sLun = "";
                                  $s1 = "";
                                  $s2 = "";
                                  $s3 = "";
                                  $s4 = "";
                                  $s5 = "";
                                  $s6 = "";
                                  $s7 = "";
                                  $s8 = "";
                                  $s9 = "";
                              }else if($var == "Bosque"){
                                  $sCap = "";
                                  $sSem = "";
                                  $sBos = "selected";
                                  $sLun = "";
                                  $s1 = "";
                                  $s2 = "";
                                  $s3 = "";
                                  $s4 = "";
                                  $s5 = "";
                                  $s6 = "";
                                  $s7 = "";
                                  $s8 = "";
                                  $s9 = "";
                              }else if($var == "Luna"){
                                  $sCap = "";
                                  $sSem = "";
                                  $sBos = "";
                                  $sLun = "selected";
                                  $s1 = "";
                                  $s2 = "";
                                  $s3 = "";
                                  $s4 = "";
                                  $s5 = "";
                                  $s6 = "";
                                  $s7 = "";
                                  $s8 = "";
                                  $s9 = "";
                              }else if($var == "1"){
                                  $sCap = "";
                                  $sSem = "";
                                  $sBos = "";
                                  $sLun = "";
                                  $s1 = "selected";
                                  $s2 = "";
                                  $s3 = "";
                                  $s4 = "";
                                  $s5 = "";
                                  $s6 = "";
                                  $s7 = "";
                                  $s8 = "";
                                  $s9 = "";
                              }else if($var == "2"){
                                  $sCap = "";
                                  $sSem = "";
                                  $sBos = "";
                                  $sLun = "";
                                  $s1 = "";
                                  $s2 = "selected";
                                  $s3 = "";
                                  $s4 = "";
                                  $s5 = "";
                                  $s6 = "";
                                  $s7 = "";
                                  $s8 = "";
                                  $s9 = "";
                              }else if($var == "3"){
                                  $sCap = "";
                                  $sSem = "";
                                  $sBos = "";
                                  $sLun = "";
                                  $s1 = "";
                                  $s2 = "";
                                  $s3 = "selected";
                                  $s4 = "";
                                  $s5 = "";
                                  $s6 = "";
                                  $s7 = "";
                                  $s8 = "";
                                  $s9 = "";
                              }else if($var == "4"){
                                  $sCap = "";
                                  $sSem = "";
                                  $sBos = "";
                                  $sLun = "";
                                  $s1 = "";
                                  $s2 = "";
                                  $s3 = "";
                                  $s4 = "selected";
                                  $s5 = "";
                                  $s6 = "";
                                  $s7 = "";
                                  $s8 = "";
                                  $s9 = "";
                              }else if($var == "5"){
                                  $sCap = "";
                                  $sSem = "";
                                  $sBos = "";
                                  $sLun = "";
                                  $s1 = "";
                                  $s2 = "";
                                  $s3 = "";
                                  $s4 = "";
                                  $s5 = "selected";
                                  $s6 = "";
                                  $s7 = "";
                                  $s8 = "";
                                  $s9 = "";
                              }else if($var == "6"){
                                  $sCap = "";
                                  $sSem = "";
                                  $sBos = "";
                                  $sLun = "";
                                  $s1 = "";
                                  $s2 = "";
                                  $s3 = "";
                                  $s4 = "";
                                  $s5 = "";
                                  $s6 = "selected";
                                  $s7 = "";
                                  $s8 = "";
                                  $s9 = "";
                              }else if($var == "7"){
                                  $sCap = "";
                                  $sSem = "";
                                  $sBos = "";
                                  $sLun = "";
                                  $s1 = "";
                                  $s2 = "";
                                  $s3 = "";
                                  $s4 = "";
                                  $s5 = "";
                                  $s6 = "";
                                  $s7 = "selected";
                                  $s8 = "";
                                  $s9 = "";
                              }else if($var == "8"){
                                  $sCap = "";
                                  $sSem = "";
                                  $sBos = "";
                                  $sLun = "";
                                  $s1 = "";
                                  $s2 = "";
                                  $s3 = "";
                                  $s4 = "";
                                  $s5 = "";
                                  $s6 = "";
                                  $s7 = "";
                                  $s8 = "selected";
                                  $s9 = "";
                              }else if($var == "9"){
                                  $sCap = "";
                                  $sSem = "";
                                  $sBos = "";
                                  $sLun = "";
                                  $s1 = "";
                                  $s2 = "";
                                  $s3 = "";
                                  $s4 = "";
                                  $s5 = "";
                                  $s6 = "";
                                  $s7 = "";
                                  $s8 = "";
                                  $s9 = "selected";
                              }
                            ?>
                            <div class="row margin pt-3" id="jardin">
                              <div class="input-field col s0">
                                <i class="material-icons prefix mrn-50 pt-5">people_outline</i>
                              </div>
                              <div class="input-field col s12">
                                <i class="prefix"></i>
                                <select class="mtn-30" id="anios_jardin" name="grado">
                                  <option selected="true" disabled="disabled">Grado</option>
                                  <option value="Capullo"<?php echo $sCap;?>>Capullo</option>
                                  <option value="Semilla"<?php echo $sSem;?>>Semilla</option>
                                  <option value="Bosque"<?php echo $sBos;?>>Bosque</option>
                                  <option value="Luna"<?php echo $sLun;?>>Luna</option>
                                </select>
                              </div>
                            </div>

                            <div class="row margin pt-3" id="primaria">
                              <div class="input-field col s0">
                                <i class="material-icons prefix mrn-50 pt-5">people_outline</i>
                              </div>
                              <div class="input-field col s12">
                                <i class="prefix"></i>
                                <select class="mtn-30" id="anios_primaria" name="grado">
                                  <option selected="true" disabled="disabled">Grado</option>
                                  <option value="1"<?php echo $s1;?>>1</option>
                                  <option value="2"<?php echo $s2;?>>2</option>
                                  <option value="3"<?php echo $s3;?>>3</option>
                                  <option value="4"<?php echo $s4;?>>4</option>
                                  <option value="5"<?php echo $s5;?>>5</option>
                                </select>
                              </div>
                            </div>

                            <div class="row margin pt-3" id="secundaria">
                              <div class="input-field col s0">
                                <i class="material-icons prefix mrn-50 pt-5">people_outline</i>
                              </div>
                              <div class="input-field col s12">
                                <i class="prefix"></i>
                                <select class="mtn-30" id="anios_secundaria" name="grado">
                                  <option selected="true" disabled="disabled">Grado</option>
                                  <option value="6"<?php echo $s6;?>>6</option>
                                  <option value="7"<?php echo $s7;?>>7</option>
                                  <option value="8"<?php echo $s8;?>>8</option>
                                  <option value="9"<?php echo $s9;?>>9</option>
                                </select>
                              </div>
                            </div>

                            <?php
                              $vargpo = $alumno->grupo;
                              //print_r($var);
                              if($vargpo == "A"){
                                  $sa = "selected";
                                  $sb = "";
                                  $sc = "";
                                  $sd = '';
                              }else if($vargpo == "B"){
                                  $sa = "";
                                  $sb = "selected";
                                  $sc = "";
                                  $sd = '';
                              }else if($vargpo == "C"){
                                  $sa = "";
                                  $sb = "";
                                  $sc = "selected";
                                  $sd = '';
                              }else if($vargpo == "D"){
                                  $sa = "";
                                  $sb = "";
                                  $sc = "";
                                  $sd = 'selected';
                              }
                            ?>
                            <div class="row margin pt-3">
                              <div class="input-field col s0">
                                <i class="material-icons prefix mrn-50 pt-5">people_outline</i>
                              </div>
                              <div class="input-field col s12">
                                <i class="prefix"></i>
                                <select class="mtn-30" name="grupo">
                                  <option selected="true" disabled="disabled">Grupo</option>
                                  <option value="A"<?php echo $sa;?>>A</option>
                                  <option value="B"<?php echo $sb;?> disabled="disabled">B</option>
                                  <option value="C"<?php echo $sc;?> disabled="disabled">C</option>
                                  <option value="D"<?php echo $sd;?> disabled="disabled">D</option>
                                </select>
                              </div>
                            </div>
                            
                            <div class="row">
                              <div class="input-field col s12">
                                <button type="submit" class="btn waves-effect waves-light col s12 fondo-lila texto-amarillo">
                                    {{ __('Actualizar alumno') }}
                                </button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--end container-->
          </div>
        </section>
        <!-- END CONTENT -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END WRAPPER -->
      </div>
    <!-- END MAIN -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
    <footer id="footer1" class="fondo-amarillo texto-gris-oscuro pt-1">
      <div class="footer-copyright">
        <div class="container">
          <span>Copyright ©
            <script type="text/javascript">
              document.write(new Date().getFullYear());
            </script> <a href="https://touchesbegan.com/" target="_blank">TOUCHESBEGAN</a> Todos los derechos reservados.</span>
          <span class="right hide-on-small-only"> Diseñado y desarrollado por <a href="https://touchesbegan.com/">TOUCHESBEGANMX</a></span>
        </div>
      </div>
    </footer>
    <!-- END FOOTER -->
    <!-- ================================================
    Scripts
    ================================================ -->
    <!-- jQuery Library -->
    <script type="text/javascript" src="{{ asset('assets/vendors/jquery-3.2.1.min.js') }}"></script>
    <!--materialize js-->
    <script type="text/javascript" src="{{ asset('assets/js/materialize.min.js') }}"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="{{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <!-- masonry -->
    <script src="{{ asset('assets/vendors/masonry.pkgd.min.js') }}"></script>
    <!-- imagesloaded -->
    <script src="{{ asset('assets/vendors/imagesloaded.pkgd.min.js') }}"></script>
    <!-- magnific-popup -->
    <script type="text/javascript" src="{{ asset('assets/vendors/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="{{ asset('assets/js/plugins.js') }}"></script>
    <!--media-gallary-page.js - Page specific JS-->
    <script type="text/javascript" src="{{ asset('assets/js/scripts/media-gallary-page.js') }}"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="{{ asset('assets/js/custom-script.js') }}"></script>
    <!--mi-script.js - Page specific JS-->
    <script type="text/javascript" src="{{ asset('assets/js/mi-script.js') }}"></script>
    <script type="text/javascript">
      if (document.getElementById('nivelgdo').value == "Jardín") {
        $("#jardin").show();
        $("#primaria").hide();
        $("#secundaria").hide();
      }else if (document.getElementById('nivelgdo').value == "Primaria") {
        $("#jardin").hide();
        $("#primaria").show();
        $("#secundaria").hide();
      }else if (document.getElementById('nivelgdo').value == "Secundaria") {
        $("#jardin").hide();
        $("#primaria").hide();
        $("#secundaria").show();
      }
    </script>
    <script type="text/javascript">
      if (document.getElementById('tutor1').value == "") {
        $("#uno").hide();
        $("#dos").hide();
      }
      if (document.getElementById('tutor2').value == "") {
        $("#uno").hide();
        $("#dos").hide();
      }
    </script>
  </body>
</html>
