<!DOCTYPE html>
<html lang="en">
<!--================================================================================
DESARROLLADO POR NATOZ -> 11/2018   ñ.ñ
================================================================================ -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Inlakesh - Cargar datos</title>
    <!-- Favicons-->
    <link rel="icon" href="{{ asset('assets/images/favicon/favicon-32x32.png') }}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('assets/images/favicon/apple-touch-icon-152x152.png') }}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link href="{{ asset('assets/css/themes/collapsible-menu/materialize.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/collapsible-menu/style.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/collapsible-menu/mi-estilo.css') }}" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="{{ asset('assets/css/custom/custom.css') }}" type="text/css" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/magnific-popup/magnific-popup.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/flag-icon/css/flag-icon.min.css') }}" type="text/css" rel="stylesheet">
  </head>
  <body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color fondo-lila">
          <div class="nav-wrapper">
            <ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen">
                  <i class="material-icons">settings_overscan</i>
                </a>
              </li>
            </ul>
            <!-- profile-dropdown -->
          </div>
        </nav>
      </div>
    </header>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <div id="fondo-gris" class="fondo-gris">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav" class="nav-lock nav-collapsible">
          <div class="brand-sidebar fondo-lila">
            <h1 class="logo-wrapper">
              <a href="{{ route('calendario_administrador') }}" class="brand-logo darken-1">
                <img src="{{ asset('assets/images/logo/materialize-logo.png') }}" alt="Inlakesh">
                <span class="logo-text hide-on-med-and-dow"><img src="{{ asset('assets/images/logo/Inlakesh_logo.png') }}" alt="Inlakesh"></span>
              </a>
              <a href="#" class="navbar-toggler">
                <i class="material-icons">radio_button_checked</i>
              </a>
            </h1>
          </div>
          <ul id="slide-out" class="side-nav fixed leftside-navigation ps-container ps-active-y fondo-morado" style="transform: translateX(-100%);">
            <li class="no-padding">
              <ul class="collapsible" data-collapsible="accordion">
                <li class="bold">
                  <a class="collapsibl-header waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">person_outline</i>
                    <span class="nav-text texto-amarillo">{{ Auth::user()->name }}</span>
                  </a>
                </li>
                <li class="bold pl-8 pr-8">
                <div class="divider"></div>
                </li>
                <li class="bold">
                  <a href="{{ route('calendario_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">today</i>
                    <span class="nav-text texto-amarillo">Calendario</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('boletin_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">assignment</i>
                    <span class="nav-text texto-amarillo">Boletin</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('galeria_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">perm_media</i>
                    <span class="nav-text texto-amarillo">Galería</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('videos_administrador') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">movie</i>
                    <span class="nav-text texto-amarillo">Videos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('alumnos') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">group_outline</i>
                    <span class="nav-text texto-amarillo">Alumnos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('tutores') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">group_outline</i>
                    <span class="nav-text texto-amarillo">Padres</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('profesores') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">group_outline</i>
                    <span class="nav-text texto-amarillo">Maestros</span>
                  </a>
                </li>
                <li class="bold active">
                  <a href="{{ route('cargar_datos') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">archive</i>
                    <span class="nav-text texto-amarillo">Cargar datos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('documentos_admin') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">library_books</i>
                    <span class="nav-text texto-amarillo">Documentos</span>
                  </a>
                </li>
                <li class="bold">
                  <a href="{{ route('avisos_admin') }}" class="waves-effect waves-cyan">
                    <i class="material-icons texto-amarillo">description</i>
                    <span class="nav-text texto-amarillo">Avisos</span>
                  </a>
                </li>
                <li class="bold pl-8 pr-8">
                <div class="divider"></div>
                </li>
                <li class="bold">
                  <a class="dropdow-item waves-effect waves-cyan texto-amarillo" href="{{ route('logout') }}"
                     onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      <i class="material-icons texto-amarillo">keyboard_tab</i>
                     <span class="nav-text texto-amarillo">{{ __('Cerrar sesión') }}</span>
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </li>
              </ul>
            </li>
          <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 609px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 293px;"></div></div></ul>
          <a href="advance-ui-transitions#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only gradient-45deg-light-blue-cyan gradient-shadow">
            <i class="material-icons">menu</i>
          </a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content">
          <!--start container-->
          <div class="container">
            <div class="section">
            <h4 class="texto-gris fuente-waldorf">Cargar datos</h4>
              <div class="divider"></div>
              <div class="section">
                <div class="row pl-2">
                  <h5 class="texto-gris fuente-waldorf">Registra los alumnos al sistema del colegio:</h5>
                </div>
                <div class="row pl-2">
                  <p class="texto-gris">Haz el primer registro en el sitio, puedes hacer el registro de "alumnos" a través de un archivo con extensión "*.csv"</p>
                  <ul>
                    <li>1.- 
                      <a href="{{ url('/subir_db/alumnos') }}">
                        <button class="btn fondo-lila texto-amarillo">Cargar datos de alumnos</button>
                      </a>
                      <?php print_r($cargaCompletaA); ?>
                    </li>
                  </ul>
                </div>
                <div class="divider"></div>
                <div class="row pl-2">
                  <h5 class="texto-gris fuente-waldorf">Registrar los padres al sistema del colegio:</h5>
                </div>
                <div class="row pl-2">
                  <p class="texto-gris">Haz el primer registro en el sitio, puedes hacer el registro de "padres" a través de un archivo con extensión "*.csv", NOTA: NO PUEDE HABER ESPACIOS EN LOS MAIL A REGISTRAR YA QUE ESTOS SERAN RECONOCIDOS EN LA INSERCIÓN A LA BASE DE DATOS</p>
                  <ul>
                    <li>1.- 
                      <a href="{{ url('/subir_db/tutores') }}">
                        <button class="btn fondo-lila texto-amarillo">Cargar datos de padres</button>
                      </a>
                      <?php print_r($cargaCompletaT); ?>
                    </li>
                    <li class="pt-2">
                      <form action="{{ url('/generar_password') }}" method="post">
                        @csrf
                        <input type="text" name="tabla" value="tutores" hidden>
                        3.- <button class="btn fondo-lila texto-amarillo">Generar contraseñas para todos los padres</button>
                      </form>
                      <?php print_r($done); ?>
                    </li>
                    <li class="pt-2">
                      <form action="{{ url('/envio_mail_tutor') }}" method="post">
                        @csrf
                        <input type="text" name="tutores_profesores" value="tutores" hidden>
                        4.- <button class="btn fondo-lila texto-amarillo">Enviar accesos a todos los padres</button>
                      </form>
                      <?php print_r($sendTut); ?>
                    </li>
                    <li class="pt-2">
                      <form action="{{ url('/dar_acceso') }}" method="post">
                        @csrf
                        <input type="text" name="tabla" value="tutores" hidden>
                        5.- <button class="btn fondo-lila texto-amarillo">Permitir acceso al sistema a padres</button>
                      </form>
                      <?php print_r($accesTut); ?>
                    </li>
                  </ul>
                </div>
                <div class="divider"></div>
                <div class="row pl-2">
                  <h5 class="texto-gris fuente-waldorf">Relaciona alumnos con padres:</h5>
                </div>
                <div class="row pl-2">
                  <p class="texto-gris">Una vez capturados los datos de los alumnos inscritos y los datos de los padres registrados dentro del sistema, ahora procede a hacer una relacion entre alumnos - padres; esto quiere decir que debes asignarle a cada alumno uno, máximo dos padres, por otra parte los padres pueden tener mínimo un alumno a su cargo.</p>
                  <ul>
                    <li>1.- 
                      <a href="{{ url('/relacionar_hijo_tutor') }}">
                        <button class="btn fondo-lila texto-amarillo">Relaciona un hijo con su(s) padre(s)</button>
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="divider"></div>
                <div class="row pl-2">
                  <h5 class="texto-gris fuente-waldorf">Registra los maestros al sistema del colegio:</h5>
                </div>
                <div class="row pl-2 pb-5">
                  <p class="texto-gris">Haz el primer registro en el sitio, puedes hacer el registro de maestros a través de un archivo con extensión "*.csv", NOTA: NO PUEDE HABER ESPACIOS EN LOS MAIL A REGISTRAR YA QUE ESTOS SERAN RECONOCIDOS EN LA INSERCIÓN A LA BASE DE DATOS</p>
                  <ul>
                    <li>1.- 
                      <a href="{{ url('/subir_db/profesor') }}">
                        <button class="btn fondo-lila texto-amarillo">Cargar datos de maestros</button>
                      </a>
                      <?php print_r($cargaCompletaP); ?>
                    </li>
                    <li class="pt-2">
                      <form action="{{ url('/generar_password') }}" method="post">
                        @csrf
                        <input type="text" name="tabla" value="profesor" hidden>
                        2.- <button class="btn fondo-lila texto-amarillo">Generar contraseñas para todos los maestros</button>
                      </form>
                      <?php print_r($doneDos); ?>
                    </li>
                    <li class="pt-2">
                      <form action="{{ url('/envio_mail_profesor') }}" method="post">
                        @csrf
                        <input type="text" name="tutores_profesores" value="profesor" hidden>
                        3.- <button class="btn fondo-lila texto-amarillo">Enviar accesos a todos los maestros</button>
                      </form>
                      <?php print_r($sendProf); ?>
                    </li>
                    <li class="pt-2">
                      <form action="{{ url('/dar_acceso') }}" method="post">
                        @csrf
                        <input type="text" name="tabla" value="profesor" hidden>
                        4.- <button class="btn fondo-lila texto-amarillo">Permitir acceso al sistema a maestros</button>
                      </form>
                      <?php print_r($accesProf); ?>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!--end container-->
          </div>
        </section>
        <!-- END CONTENT -->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        </div>
        <!-- END WRAPPER -->
      </div>
    <!-- END MAIN -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
    <footer id="footer1" class="fondo-amarillo texto-gris-oscuro pt-1">
      <div class="footer-copyright">
        <div class="container">
          <span>Copyright ©
            <script type="text/javascript">
              document.write(new Date().getFullYear());
            </script> <a href="https://touchesbegan.com/" target="_blank">TOUCHESBEGAN</a> Todos los derechos reservados.</span>
          <span class="right hide-on-small-only"> Diseñado y desarrollado por <a href="https://touchesbegan.com/">TOUCHESBEGANMX</a></span>
        </div>
      </div>
    </footer>
    <!-- END FOOTER -->
    <!-- ================================================
    Scripts
    ================================================ -->
    <!-- jQuery Library -->
    <script type="text/javascript" src="{{ asset('assets/vendors/jquery-3.2.1.min.js') }}"></script>
    <!--materialize js-->
    <script type="text/javascript" src="{{ asset('assets/js/materialize.min.js') }}"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="{{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <!-- masonry -->
    <script src="{{ asset('assets/vendors/masonry.pkgd.min.js') }}"></script>
    <!-- imagesloaded -->
    <script src="{{ asset('assets/vendors/imagesloaded.pkgd.min.js') }}"></script>
    <!-- magnific-popup -->
    <script type="text/javascript" src="{{ asset('assets/vendors/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="{{ asset('assets/js/plugins.js') }}"></script>
    <!--media-gallary-page.js - Page specific JS-->
    <script type="text/javascript" src="{{ asset('assets/js/scripts/media-gallary-page.js') }}"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="{{ asset('assets/js/custom-script.js') }}"></script>
  </body>
</html>
