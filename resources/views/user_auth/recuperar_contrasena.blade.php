<!DOCTYPE html>
<html lang="en">
<!--================================================================================
DESARROLLADO POR NATOZ -> 11/2018   ñ.ñ
================================================================================ -->
  <head>
    <title>Inlakesh</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <!-- Favicons-->
    <link rel="icon" href="{{ asset('assets/images/favicon/favicon-32x32.png') }}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('assets/images/favicon/apple-touch-icon-152x152.png') }}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->
    <!-- CORE CSS-->
    <link href="{{ asset('assets/css/themes/collapsible-menu/materialize.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/collapsible-menu/style.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/collapsible-menu/mi-estilo.css') }}" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="{{ asset('assets/css/custom/custom.css" type="text/css') }}" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jvectormap/jquery-jvectormap.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/flag-icon/css/flag-icon.min.css') }}" type="text/css" rel="stylesheet">
  </head>
  <body>
    <!-- Visualizador de carga -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color gradient-45deg-purple-deep-orange gradient-shadow navega768">
          <div class="nav-wrapper">          
            <ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen">
                  <i class="material-icons">settings_overscan</i>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </header>
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="fondo-main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav" class="nav-expanded nav-lock">
          <div class="brand-sidebar">
            <h1 class="logo-wrapper">
              <a href="/" class="brand-logo darken-1">
                <img src="{{ asset('assets/images/logo/materialize-logo.png') }}" alt="Inlakesh">
                <span class="logo-text hide-on-md-and-down"><img src="{{ asset('assets/images/logo/Inlakesh_logo.png') }}" alt="Inlakesh"></span>
              </a>
            </h1>
          </div>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- START CONTENT -->
          <!--start container-->
          <div class="color-inlakesh formulario">
            <div id="login-page" class="row pt-4 center">
              <div class="col s4"></div>
              <div class="col s4">
                <form method="POST" action="{{ route('password.email') }}">
                  @csrf
                  <div class="form-group row margin center">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo electrónico') }}</label>
                      <div class="col s12">
                          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                          @error('email')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                      </div>
                  </div>
                  <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                      <button type="submit" class="btn btn-primary">
                        {{ __('Enviar link para restaurar contraseña') }}
                      </button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="col s4"></div>
            </div>
          </div>
        <!-- END CONTENT -->
        <!-- END RIGHT SIDEBAR NAV-->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START FOOTER -->
      <footer id="footer1" style="padding-left: 0px; padding-top: 8px; padding-bottom: 8px;" class="page-footer gradient-45deg-purple-deep-orange">
        <div class="footer-copyright">
          <div class="container">
            <span>Copyright ©
              <script type="text/javascript">
                document.write(new Date().getFullYear());
              </script> <a class="grey-text text-lighten-4" href="https://touchesbegan.com/" target="_blank">TOUCHESBEAN</a> Todos los derechos reservados.</span>
            <span class="right hide-on-small-only"> Diseñado y desarrollado por <a class="grey-text text-lighten-4" href="https://touchesbegan.com/">TOUCHESBEGANMX</a></span>
          </div>
        </div>
      </footer>
      <!-- END FOOTER -->
      <!-- ================================================
    Scripts
    ================================================ -->
      <!-- jQuery Library -->
      <script type="text/javascript" src="{{ asset('assets/vendors/jquery-3.2.1.min.js') }}"></script>
      <!--materialize js-->
      <script type="text/javascript" src="{{ asset('assets/js/materialize.min.js') }}"></script>
      <!--prism-->
      <script type="text/javascript" src="{{ asset('assets/vendors/prism/prism.js') }}"></script>
      <!--scrollbar-->
      <script type="text/javascript" src="{{ asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
      <!-- chartjs -->
      <script type="text/javascript" src="{{ asset('assets/vendors/chartjs/chart.min.js') }}"></script>
      <!--plugins.js - Some Specific JS codes for Plugin Settings-->
      <script type="text/javascript" src="{{ asset('assets/js/plugins.js') }}"></script>
      <!--custom-script.js - Add your own theme custom JS-->
      <script type="text/javascript" src="{{ asset('assets/js/custom-script.js') }}"></script>
      <script type="text/javascript" src="{{ asset('assets/js/scripts/dashboard-ecommerce.js') }}"></script>
  </body>
</html>
